#include "glh_app_process.h"
#include "main.h"

#define MODE_PC0 OB_SWD_PB6_GPIO_PC0

/**
  * @brief  写option空间函数
  * @param  无
  * @retval 无
  */
static void APP_FlashOBProgram(void)
{
    FLASH_OBProgramInitTypeDef OBInitCfg;

    HAL_FLASH_Unlock();        /* 解锁FLASH */
    HAL_FLASH_OB_Unlock();     /* 解锁OPTION */
  
    OBInitCfg.OptionType = OPTIONBYTE_USER;
    OBInitCfg.USERType = OB_USER_BOR_EN | OB_USER_BOR_LEV | OB_USER_IWDG_SW | OB_USER_IWDG_STOP | OB_USER_SWD_NRST_MODE;

    OBInitCfg.USERConfig = OB_BOR_DISABLE | OB_BOR_LEVEL_3p1_3p2 | OB_IWDG_SW | OB_IWDG_STOP_ACTIVE | MODE_PC0 ;

    /* 启动option byte编程 */
    HAL_FLASH_OBProgram(&OBInitCfg);

    HAL_FLASH_Lock();      /* 锁定FLASH */
    HAL_FLASH_OB_Lock();   /* 锁定OPTION */

    /* 产生一个复位，option byte装载 */
    HAL_FLASH_OB_Launch();
}

int main(void)
{
    if(READ_BIT(FLASH->OPTR, OB_USER_SWD_NRST_MODE)!= MODE_PC0)    //复位脚配置成普通IO口
    {
        APP_FlashOBProgram();
    }
    
    GLHAPS_Init();

    while (1)
    {
        GLHAPS_MainThread();
    }
}
