/*
* Copyright (c) 2023, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_app_process.c
* 文件标识：
* 摘 要：
*   主要对私有化的任务做处理
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2023年3月31日
*/
#include "glh_app_process.h"
#include "glh_sys_tick.h"
#include "glh_led.h"
#include "glh_key.h"
#include "glh_pwm.h"
#include "glh_adc.h"
#include "glh_solenoid.h"
#include "main.h"

#define STRENGTH_LEVEL1_PWM      120
#define STRENGTH_LEVEL2_PWM      160
#define STRENGTH_LEVEL3_PWM      190

#define LED_STRENGTH_LEVEL1_FLAG    (LED1_FLAG)
#define LED_STRENGTH_LEVEL2_FLAG    (LED2_FLAG)
#define LED_STRENGTH_LEVEL3_FLAG    (LED3_FLAG)
#define LED_HOT_LEVEL1_FLAG         (LED4_FLAG)
#define LED_HOT_LEVEL2_FLAG         (LED5_FLAG)
#define LED_HOT_LEVEL3_FLAG         (LED6_FLAG)
#define LED_VAC_FLAG                LED7_FLAG
#define ORANGE_LED_BAT_FLAG         LED8_FLAG
#define WHITE_LED_BAT_FLAG          LED9_FLAG

#define CHARGE_CHECK_PORT           GPIOB
#define CHARGE_CHECK_PIN            LL_GPIO_PIN_4
#define BOST_PORT                   GPIOB
#define BOST_PIN                    LL_GPIO_PIN_5
#define CHARGE_STATUS_PORT          GPIOB                 //跟电磁阀控制IO口复用
#define CHARGE_STATUS_PIN           LL_GPIO_PIN_3
#define SOLENOID_CTR_PORT           GPIOB
#define SOLENOID_CTR_PIN            LL_GPIO_PIN_3
#define SOLENOID_CHECK_PORT         GPIOC
#define SOLENOID_CHECK_PIN          LL_GPIO_PIN_0
#define MOTOR_CHECK_PORT            GPIOC
#define MOTOR_CHECK_PIN             LL_GPIO_PIN_1
#define HOT_PORT                    GPIOA
#define HOT_PIN                     LL_GPIO_PIN_5
#define HOT_CHECK_PORT              GPIOA
#define HOT_CHECK_PIN               LL_GPIO_PIN_6

#define BATTERY_VOLTAGE_SLIDE_FILTERING_BUFF_LEN  130           //电池电压滑动滤波缓存长度

#define NTC_OPEN_TEMPRATURE             (-300.0)                  //开路时，检测温度返回此值
#define NTC_SHORT_TEMPRATURE            (-400.0)

#define CHARGE_FULL_VOLTAGE             4.1                     //充满插线4.185     充满拔线4.185   插线充电过程中4.24
#define POWER_ALARM_VOLTAGE             3.2
#define POWER_OFF_VOLTAGE               3.0

#define HOT_LEVEL1_FULL_TEMPRATURE      33
#define HOT_LEVEL1_STOP_TEMPRATURE      37
#if((HOT_LEVEL1_FULL_TEMPRATURE > HOT_LEVEL1_STOP_TEMPRATURE) || (HOT_LEVEL1_FULL_TEMPRATURE == HOT_LEVEL1_STOP_TEMPRATURE))
#error  HOT_LEVEL1_FULL_TEMPRATURE 不比 HOT_LEVEL1_STOP_TEMPRATURE小，不行
#endif

#define HOT_LEVEL2_FULL_TEMPRATURE      38
#define HOT_LEVEL2_STOP_TEMPRATURE      42
#if((HOT_LEVEL2_FULL_TEMPRATURE > HOT_LEVEL2_STOP_TEMPRATURE) || (HOT_LEVEL2_FULL_TEMPRATURE == HOT_LEVEL2_STOP_TEMPRATURE))
#error  HOT_LEVEL2_FULL_TEMPRATURE 不比 HOT_LEVEL2_STOP_TEMPRATURE小，不行
#endif


#define HOT_LEVEL3_FULL_TEMPRATURE     43
#define HOT_LEVEL3_STOP_TEMPRATURE     47
#if((HOT_LEVEL3_FULL_TEMPRATURE > HOT_LEVEL3_STOP_TEMPRATURE) || (HOT_LEVEL3_FULL_TEMPRATURE == HOT_LEVEL3_STOP_TEMPRATURE))
#error  HOT_LEVEL3_FULL_TEMPRATURE 不比 HOT_LEVEL3_STOP_TEMPRATURE小，不行
#endif

#define MOTOR_SOLENOID_CHECK_AD_TIMES 66

typedef enum _SWITCH_STATUS_E
{
    E_SWITCH_STATUS_OFF = 0,
	E_SWITCH_STATUS_ON,
} SWITCH_STATUS_E;

typedef enum _SUCTION_STATUS_E
{
    E_SUCTION_STATUS_OFF = 0,
    E_SUCTION_STATUS_ON,
}SUCTION_STATUS_E;

typedef enum _SUCTION_STRENGTH_E
{
    E_SUCTION_STRENGTH_LEVEL_MIN = 0,
    E_SUCTION_STRENGTH_LEVEL1,
    E_SUCTION_STRENGTH_LEVEL2,
    E_SUCTION_STRENGTH_LEVEL3,
    E_SUCTION_STRENGTH_LEVEL_MAX
}SUCTION_STRENGTH_E;

typedef enum _HOT_STATUS_E
{
    E_HOT_STATUS_ON,
    E_HOT_STATUS_OFF,
}E_HOT_STATUS;

typedef enum _HOT_LEVLE_E
{
    E_HOT_LEVEL_MIN = 0,
    E_HOT_LEVEL_LEVEL1,
    E_HOT_LEVEL_LEVEL2,
    E_HOT_LEVEL_LEVEL3,
    E_HOT_LEVEL_MAX,
}HOT_LEVEL_E;

typedef enum _LED_CIRCLE_INDICATE_E
{
    E_LED_CIRCLE_INDICATE_UNKOWN = 0,
    E_LED_CIRCLE_INDICATE_SUCTION,
    E_LED_CIRCLE_INDICATE_HOT,
}LED_CIRCLE_INDICATE_E;

typedef enum _POWER_SATTE_E
{
    E_PWOER_STATE_NOMAL = 0,
    E_PWOER_STATE_ALARM,
    E_PWOER_STATE_STOP_WORKING,
}POWER_SATTE_E;

typedef struct _SC_CONFIG_S
{
    SWITCH_STATUS_E eSwitch;               //开关状态
    SUCTION_STATUS_E eSuctionStatus;
    SUCTION_STRENGTH_E eSuctionStrength;
    E_HOT_STATUS eHotStatus;
    HOT_LEVEL_E eHotLevl;
    LED_CIRCLE_INDICATE_E eLedCircleIndicate;
    POWER_SATTE_E ePowerState;
} SC_CONFIG_S;

SC_CONFIG_S s_sScConfig;
static BOOL s_bIsChargingFlag = FALSE;
static BOOL s_bIsChargeFullFlag = FALSE;
static uint32 s_u32ReleaseInterval = 0;
float fBatVoatage = CHARGE_FULL_VOLTAGE;
static BOOL BatVoatageInitFlag = FALSE;
float fBatteryVoltageSlideFilteringBuff[BATTERY_VOLTAGE_SLIDE_FILTERING_BUFF_LEN] = {0};
static uint32 s_u32LastBostSendTime = 0;
static uint32 s_u32IsNeedBostSend = FALSE;
static uint32 s_u32PowerOffStartTime = 0;
static uint32 s_u32WakeUpTime = 0;
static BOOL s_bIsNeedFullHotFlag1 = FALSE, s_bIsNeedFullHotFlag2 = FALSE, s_bIsNeedFullHotFlag3 = FALSE;
static uint32 s_u32NeedFullStartTime = 0;
static uint32 s_u32VoltageUpStartTime = 0;       //升压开始的时间
static uint32 s_u32SuctionStrengthAdjustTime = 0;
volatile BOOL bReleaseKeyDownFlag = FALSE;
volatile uint32 u32ReleaseKeyUpTime = 0;
volatile BOOL bSolenoidErrorFlag = FALSE;
volatile uint32 u32SolenoidErrorStartTime = 0;
static uint8 s_u8SolenoidCheckCnt = 0;

CODE float afNtcRes[126] = 
{
28.007,
26.8,
25.651,
24.558,
23.517,
22.526,
21.582,
20.682,
19.825,
19.008,
18.229,
17.485,
16.777,
16.1,
15.455,
14.838,
14.25,
13.688,
13.151,
12.638,
12.148,
11.679,
11.231,
10.802,
10.392,
10,
9.625,
9.265,
8.921,
8.592,
8.276,
7.974,
7.684,
7.406,
7.14,
6.885,
6.64,
6.405,
6.18,
5.963,
5.756,
5.556,
5.365,
5.181,
5.005,
4.835,
4.672,
4.515,
4.365,
4.22,
4.081,
3.947,
3.818,
3.694,
3.574,
3.459,
3.349,
3.242,
3.14,
3.041,
2.945,
2.854,
2.765,
2.68,
2.598,
2.518,
2.442,
2.368,
2.297,
2.228,
2.162,
2.098,
2.036,
1.976,
1.919,
1.863,
1.809,
1.757,
1.707,
1.658,
1.612,
1.566,
1.522,
1.48,
1.439,
1.399,
1.361,
1.324,
1.288,
1.253,
1.219,
1.186,
1.155,
1.124,
1.094,
1.066,
1.038,
1.011,
0.985,
0.959,
0.935,
0.911,
0.888,
0.865,
0.843,
0.822,
0.802,
0.782,
0.763,
0.744,
0.726,
0.708,
0.691,
0.674,
0.658,
0.642,
0.627,
0.612,
0.598,
0.584,
0.57,
0.557,
0.544,
0.532,
0.519,
0.508,
};

static void iSolenoidMainThread(void);
static void iBatteryVoltageCheckMainThread(void);
static void iTempratureMainThread(void);
static float iGetTemprature(void);
static void iChargeIoInit(void);
static void iChargeCheckMainThread(void);
static void iEnterSleepMode(void);
static void iBostMaintThread(void);
static void iPowerOffNoSleepCheckMainThread(void);
static void iSolenoidCheckPinInit(void);
static void iMotorCheckPinInit(void);
static void MotorSolenoidCheckMainThread(void);
static void iBostPinInit(void);
static void iHotCheckPinInit(void);

//复位模式参数
static void iResetScConfig(void)
{
    s_sScConfig.eSwitch = E_SWITCH_STATUS_OFF;
    
    s_sScConfig.eSuctionStatus = E_SUCTION_STATUS_OFF;
    s_sScConfig.eSuctionStrength = E_SUCTION_STRENGTH_LEVEL1;
    
    s_sScConfig.eHotStatus = E_HOT_STATUS_OFF;
    s_sScConfig.eHotLevl = E_HOT_LEVEL_LEVEL1;
    
    s_sScConfig.eLedCircleIndicate = E_LED_CIRCLE_INDICATE_SUCTION;
    
    s_sScConfig.ePowerState = E_PWOER_STATE_NOMAL;
}

static void iRefreshAsConfig(void)
{
    uint8 i = 0;
    uint32 u32WaitStartTime = 0;

    if(s_sScConfig.ePowerState == E_PWOER_STATE_STOP_WORKING)
    {
        GLHPWM_Set(PWM_CHANNEL_MOTOR, 0);           //关闭吸气马达
        GLHPWM_Set(PWM_CHANNEL_HOT, 0);             //关闭加热
        GLHLED_TurnOff(LED_ALL);                    //关闭所有指示灯
        GLHSOLENOID_Set(E_SOLENOID_STATUS_CLOSE);   //开启放气
        for(i=0; i<5; i++)
        {
            GLHLED_TurnOn(LED_STRENGTH_LEVEL1_FLAG | LED_STRENGTH_LEVEL2_FLAG | LED_STRENGTH_LEVEL3_FLAG | LED_HOT_LEVEL1_FLAG | LED_HOT_LEVEL2_FLAG | LED_HOT_LEVEL3_FLAG | ORANGE_LED_BAT_FLAG);
            u32WaitStartTime = GulSystickCount;
            while(GulSystickCount - u32WaitStartTime < 300)
            {
            
            }
            
            GLHLED_TurnOff(LED_STRENGTH_LEVEL1_FLAG | LED_STRENGTH_LEVEL2_FLAG | LED_STRENGTH_LEVEL3_FLAG | LED_HOT_LEVEL1_FLAG | LED_HOT_LEVEL2_FLAG | LED_HOT_LEVEL3_FLAG | ORANGE_LED_BAT_FLAG);
            u32WaitStartTime = GulSystickCount;
            while(GulSystickCount - u32WaitStartTime < 300)
            {
            
            }
        }
        s_sScConfig.eSwitch = E_SWITCH_STATUS_OFF;
        GLHPWM_Set(PWM_CHANNEL_MOTOR, 0);           //关闭吸气马达
        GLHPWM_Set(PWM_CHANNEL_HOT, 0);             //关闭加热
        GLHLED_TurnOff(LED_ALL);                    //关闭所有指示灯
        GLHSOLENOID_Set(E_SOLENOID_STATUS_CLOSE);   //开启放气
        return;
    }
    
    if(s_sScConfig.eSwitch != E_SWITCH_STATUS_ON)
    {
        GLHPWM_Set(PWM_CHANNEL_MOTOR, 0);           //关闭吸气马达
        GLHPWM_Set(PWM_CHANNEL_HOT, 0);             //关闭加热
        GLHLED_TurnOff(LED_ALL);                    //关闭所有指示灯
        GLHSOLENOID_Set(E_SOLENOID_STATUS_CLOSE);   //开启放气
        return;
    }
    
    GLHLED_TurnOn(LED1_OUTSIDE_FLAG);          //开机状态，点亮按摩头里面有12个LED灯
    if(s_sScConfig.ePowerState == E_PWOER_STATE_NOMAL)
    {
        GLHLED_TurnOn(WHITE_LED_BAT_FLAG);
        GLHLED_TurnOff(ORANGE_LED_BAT_FLAG);
    }
    else if(s_sScConfig.ePowerState == E_PWOER_STATE_ALARM)
    {
        GLHLED_TurnOn(ORANGE_LED_BAT_FLAG);
        GLHLED_TurnOff(WHITE_LED_BAT_FLAG);
    }
    
    if(s_sScConfig.eSuctionStatus == E_SUCTION_STATUS_ON)
    {
        GLHLED_TurnOn(LED_VAC_FLAG);
        
        if(s_sScConfig.eSuctionStrength == E_SUCTION_STRENGTH_LEVEL1)
        {
            GLHSOLENOID_Set(E_SOLENOID_STATUS_CLOSE); //停止放气
            
            GLHPWM_Set(PWM_CHANNEL_MOTOR, STRENGTH_LEVEL1_PWM);
            
            s_u32ReleaseInterval = 850;
            
            GLHLED_TurnOn(LED_STRENGTH_LEVEL1_FLAG);
            GLHLED_TurnOff(LED_STRENGTH_LEVEL2_FLAG | LED_STRENGTH_LEVEL3_FLAG);
        }
        else if(s_sScConfig.eSuctionStrength == E_SUCTION_STRENGTH_LEVEL2)
        {
            GLHPWM_Set(PWM_CHANNEL_MOTOR, STRENGTH_LEVEL2_PWM);
        
            s_u32ReleaseInterval = 850;
            
            GLHLED_TurnOn(LED_STRENGTH_LEVEL1_FLAG | LED_STRENGTH_LEVEL2_FLAG);
            GLHLED_TurnOff(LED_STRENGTH_LEVEL3_FLAG);
        }
        else if(s_sScConfig.eSuctionStrength == E_SUCTION_STRENGTH_LEVEL3)
        {
            GLHPWM_Set(PWM_CHANNEL_MOTOR, STRENGTH_LEVEL3_PWM); 
        
            s_u32ReleaseInterval = 850;
            
            GLHLED_TurnOn(LED_STRENGTH_LEVEL1_FLAG | LED_STRENGTH_LEVEL2_FLAG | LED_STRENGTH_LEVEL3_FLAG);
        }
    }
    else if(s_sScConfig.eSuctionStatus == E_SUCTION_STATUS_OFF)
    {
        GLHLED_TurnOff(LED_VAC_FLAG);
        GLHSOLENOID_Set(E_SOLENOID_STATUS_CLOSE);
        GLHPWM_Set(PWM_CHANNEL_MOTOR, 0);           //关闭吸气马达
        GLHLED_TurnOff(LED_STRENGTH_LEVEL1_FLAG | LED_STRENGTH_LEVEL2_FLAG | LED_STRENGTH_LEVEL3_FLAG);
    }
    
    if(s_sScConfig.eHotStatus == E_HOT_STATUS_ON)
    {
        if(s_sScConfig.eHotLevl == E_HOT_LEVEL_LEVEL1)
        {           
            GLHLED_TurnOn(LED_HOT_LEVEL1_FLAG);
            GLHLED_TurnOff(LED_HOT_LEVEL2_FLAG | LED_HOT_LEVEL3_FLAG);
        }
        else if(s_sScConfig.eHotLevl == E_HOT_LEVEL_LEVEL2)
        {
            GLHLED_TurnOn(LED_HOT_LEVEL1_FLAG | LED_HOT_LEVEL2_FLAG);
            GLHLED_TurnOff(LED_HOT_LEVEL3_FLAG);
        }
        else if(s_sScConfig.eHotLevl == E_HOT_LEVEL_LEVEL3)
        {
            GLHLED_TurnOn(LED_HOT_LEVEL1_FLAG | LED_HOT_LEVEL2_FLAG | LED_HOT_LEVEL3_FLAG);
        }
    }
    else
    {
        GLHPWM_Set(PWM_CHANNEL_HOT, 0);             //关闭加热

        GLHLED_TurnOff(LED_HOT_LEVEL1_FLAG | LED_HOT_LEVEL2_FLAG | LED_HOT_LEVEL3_FLAG);
    }  
}

static void iInitScConfig(void)
{
    iResetScConfig();
	iRefreshAsConfig();
}

void GLHAPS_Init(void)
{
	GLHST_Init();
    GLHLED_Init();
    GLHKEY_Init();
    GLHPWM_Init(PWM_CHANNEL_MOTOR | PWM_CHANNEL_HOT);
    GLHADC_Init(TEMPRATURE_ADC_CH | BATTERY_VOLTAGE_ADC_CH);
    GLHSOLENOID_Init();
    iInitScConfig();
    iChargeIoInit();
    iSolenoidCheckPinInit();
    iMotorCheckPinInit();
    iBostPinInit();
    iHotCheckPinInit();
}

void GLHAPS_MainThread(void)
{
    GLHKEY_MainThread();
    iSolenoidMainThread();
    iBatteryVoltageCheckMainThread();
    iTempratureMainThread();
    iChargeCheckMainThread();
    iBostMaintThread();
    iPowerOffNoSleepCheckMainThread();
    MotorSolenoidCheckMainThread();
}

void GLHKEY_EventCb(uint8 u8Code, uint8 u8Action)
{
    if(s_bIsChargingFlag == TRUE)       //充电状态下不处理按键
    {
        return;
    }
    
    if(s_sScConfig.ePowerState == E_PWOER_STATE_STOP_WORKING)         //低电压状态下不处理按键
    {
        if(u8Code != KEY_CODE_ON_OFF)
        {
            return;
        }
    }
    
    switch(u8Code)
    {
        case KEY_CODE_HOT:
        {
            if(u8Action == E_KEY_ACTION_SHORT_KEYDOWN)
            {
                if(s_sScConfig.eSwitch != E_SWITCH_STATUS_ON)
                {
                    return;
                }
                
                if(s_sScConfig.eHotStatus == E_HOT_STATUS_ON)
                {
                    if(s_sScConfig.eLedCircleIndicate != E_LED_CIRCLE_INDICATE_HOT)
                    {
                        s_sScConfig.eLedCircleIndicate = E_LED_CIRCLE_INDICATE_HOT;
                        iRefreshAsConfig();
                    }
                    else
                    {
                        s_sScConfig.eHotStatus = E_HOT_STATUS_OFF;
                        if(s_sScConfig.eSuctionStatus == E_SUCTION_STATUS_ON)
                        {
                            s_sScConfig.eLedCircleIndicate = E_LED_CIRCLE_INDICATE_SUCTION;
                        }
                        iRefreshAsConfig(); 
                    }
                }
                else if(s_sScConfig.eHotStatus == E_HOT_STATUS_OFF)
                {
                    s_sScConfig.eHotLevl = E_HOT_LEVEL_LEVEL1;
                    s_sScConfig.eHotStatus = E_HOT_STATUS_ON;
                    if(s_sScConfig.eHotLevl == E_HOT_LEVEL_LEVEL1)
                    {
                        if(iGetTemprature() < HOT_LEVEL1_FULL_TEMPRATURE - 3)
                        {
                            s_bIsNeedFullHotFlag1 = TRUE;
                            s_u32NeedFullStartTime = GulSystickCount;
                        }
                    }
                    else if(s_sScConfig.eHotLevl == E_HOT_LEVEL_LEVEL2)
                    {
                        if(iGetTemprature() < HOT_LEVEL2_FULL_TEMPRATURE - 3)
                        {
                                s_bIsNeedFullHotFlag2 = TRUE;
                                s_u32NeedFullStartTime = GulSystickCount;
                        }
                    }
                    else if(s_sScConfig.eHotLevl == E_HOT_LEVEL_LEVEL3)
                    {
                        if(iGetTemprature() < HOT_LEVEL3_FULL_TEMPRATURE - 3)
                        {
                            s_bIsNeedFullHotFlag3 = TRUE;
                            s_u32NeedFullStartTime = GulSystickCount;
                        }
                    }
                    s_sScConfig.eLedCircleIndicate = E_LED_CIRCLE_INDICATE_HOT;
                    iRefreshAsConfig();
                }
            }
        }
        break;
        
        case KEY_CODE_V_C:   
        {
            if(s_sScConfig.eSwitch != E_SWITCH_STATUS_ON)
            {
                return;
            }
            
            if(u8Action == E_KEY_ACTION_SHORT_KEYDOWN)
            {         
                if(s_sScConfig.eSuctionStatus == E_SUCTION_STATUS_ON)
                {
                    if(s_sScConfig.eLedCircleIndicate != E_LED_CIRCLE_INDICATE_SUCTION)
                    {
                        s_sScConfig.eLedCircleIndicate = E_LED_CIRCLE_INDICATE_SUCTION;
                        iRefreshAsConfig();
                    }
                    else
                    {
                        s_sScConfig.eSuctionStatus = E_SUCTION_STATUS_OFF;
                        if(s_sScConfig.eHotStatus == E_HOT_STATUS_ON)
                        {
                            s_sScConfig.eLedCircleIndicate = E_LED_CIRCLE_INDICATE_HOT;
                        }
                        iRefreshAsConfig();
                    }
                }
                else if(s_sScConfig.eSuctionStatus == E_SUCTION_STATUS_OFF)
                {
                    s_sScConfig.eSuctionStrength = E_SUCTION_STRENGTH_LEVEL1;
                    s_sScConfig.eSuctionStatus = E_SUCTION_STATUS_ON;
                    s_sScConfig.eLedCircleIndicate = E_LED_CIRCLE_INDICATE_SUCTION;
                    s_u32VoltageUpStartTime = GulSystickCount;
                    iRefreshAsConfig();
                    
                    if(GulSystickCount - s_u32LastBostSendTime >= 2000)
                    {
                        s_u32LastBostSendTime = GulSystickCount;
                        LL_GPIO_ResetOutputPin(BOST_PORT, BOST_PIN);
                        s_u32IsNeedBostSend = TRUE;
                    }
                }
            }
        }
        break;

        case KEY_CODE_INC:
        {
            if(s_sScConfig.eSwitch != E_SWITCH_STATUS_ON)
            {
                return;
            }
            
            if(u8Action == E_KEY_ACTION_SHORT_KEYDOWN)
            {
                if(s_sScConfig.eLedCircleIndicate == E_LED_CIRCLE_INDICATE_SUCTION)
                {
                    if(s_sScConfig.eSuctionStatus != E_SUCTION_STATUS_ON)
                    {
                        return;
                    }
                    
                    if(s_sScConfig.eSuctionStrength < E_SUCTION_STRENGTH_LEVEL_MAX - 1)
                    {
                        s_sScConfig.eSuctionStrength++;
                        iRefreshAsConfig();
                    }
                }
                else if(s_sScConfig.eLedCircleIndicate == E_LED_CIRCLE_INDICATE_HOT)
                {
                    if(s_sScConfig.eHotStatus != E_HOT_STATUS_ON)
                    {
                        return;
                    }
                    
                    if(s_sScConfig.eHotLevl == E_HOT_LEVEL_MAX - 1)
                    {
                        return;
                    }
                    
                    if(s_sScConfig.eHotLevl < E_HOT_LEVEL_MAX - 1)
                    {
                        s_sScConfig.eHotLevl++;
                        if(s_sScConfig.eHotLevl == E_HOT_LEVEL_LEVEL1)
                        {
                            if(iGetTemprature() < HOT_LEVEL1_FULL_TEMPRATURE)
                            {
                                s_bIsNeedFullHotFlag1 = TRUE;
                                s_u32NeedFullStartTime = GulSystickCount;
                            }
                        }
                        else if(s_sScConfig.eHotLevl == E_HOT_LEVEL_LEVEL2)
                        {
                            if(iGetTemprature() < HOT_LEVEL2_FULL_TEMPRATURE)
                            {
                                s_bIsNeedFullHotFlag2 = TRUE;
                                s_u32NeedFullStartTime = GulSystickCount;
                            }
                        }
                        else if(s_sScConfig.eHotLevl == E_HOT_LEVEL_LEVEL3)
                        {
                            if(iGetTemprature() < HOT_LEVEL3_FULL_TEMPRATURE)
                            {
                                s_bIsNeedFullHotFlag3 = TRUE;
                                s_u32NeedFullStartTime = GulSystickCount;
                            }
                        }
                        iRefreshAsConfig();
                    }
                }
            }
        }
        break;
        
        case KEY_CODE_DEC:   
        {
            if(s_sScConfig.eSwitch != E_SWITCH_STATUS_ON)
            {
                return;
            }
            
            if(u8Action == E_KEY_ACTION_SHORT_KEYDOWN)
            {
                if(s_sScConfig.eLedCircleIndicate == E_LED_CIRCLE_INDICATE_SUCTION)
                {
                    if(s_sScConfig.eSuctionStatus != E_SUCTION_STATUS_ON)
                    {
                        return;
                    }
                    
                    if(s_sScConfig.eSuctionStrength > E_SUCTION_STRENGTH_LEVEL_MIN + 1)
                    {
                        s_sScConfig.eSuctionStrength--;
                        iRefreshAsConfig();
                    }
                }
                else if(s_sScConfig.eLedCircleIndicate == E_LED_CIRCLE_INDICATE_HOT)
                {
                    if(s_sScConfig.eHotStatus != E_HOT_STATUS_ON)
                    {
                        return;
                    }
                    
                    if(s_sScConfig.eHotLevl == E_HOT_LEVEL_MIN + 1)
                    {
                        return;
                    }
                    
                    if(s_sScConfig.eHotLevl > E_HOT_LEVEL_MIN + 1)
                    {
                        s_sScConfig.eHotLevl--;
                        if(s_sScConfig.eHotLevl == E_HOT_LEVEL_LEVEL1)
                        {
                            if(iGetTemprature() < HOT_LEVEL1_FULL_TEMPRATURE)
                            {
                                s_bIsNeedFullHotFlag1 = TRUE;
                                s_u32NeedFullStartTime = GulSystickCount;
                            }
                        }
                        else if(s_sScConfig.eHotLevl == E_HOT_LEVEL_LEVEL2)
                        {
                            if(iGetTemprature() < HOT_LEVEL2_FULL_TEMPRATURE)
                            {
                                s_bIsNeedFullHotFlag2 = TRUE;
                                s_u32NeedFullStartTime = GulSystickCount;
                            }
                        }
                        else if(s_sScConfig.eHotLevl == E_HOT_LEVEL_LEVEL3)
                        {
                            if(iGetTemprature() < HOT_LEVEL3_FULL_TEMPRATURE)
                            {
                                s_bIsNeedFullHotFlag3 = TRUE;
                                s_u32NeedFullStartTime = GulSystickCount;
                            }
                        }
                        iRefreshAsConfig();
                    }
                }
            }
        }
        break;
        
        case KEY_CODE_ON_OFF: 
        {
            if(u8Action == E_KEY_ACTION_SHORT_KEYDOWN)
            {
                if(s_sScConfig.eSwitch == E_SWITCH_STATUS_ON)
                {      
                    s_sScConfig.eSwitch = E_SWITCH_STATUS_OFF;
                    s_u32PowerOffStartTime = GulSystickCount;
                }
                else if(s_sScConfig.eSwitch == E_SWITCH_STATUS_OFF)
                {            
                    s_sScConfig.eSwitch = E_SWITCH_STATUS_ON;
                    
                    s_sScConfig.eSuctionStatus = E_SUCTION_STATUS_ON;        //开机恢复默认参数
                    
                    s_sScConfig.eHotStatus = E_HOT_STATUS_OFF;
                    s_sScConfig.eHotLevl = E_HOT_LEVEL_LEVEL1;
                    
                    s_sScConfig.eSuctionStatus = E_SUCTION_STATUS_ON;
                    s_sScConfig.eSuctionStrength = E_SUCTION_STRENGTH_LEVEL1;
                    s_u32VoltageUpStartTime = GulSystickCount;
                    
                    s_sScConfig.eLedCircleIndicate = E_LED_CIRCLE_INDICATE_SUCTION;
                    
                    if(GulSystickCount - s_u32LastBostSendTime >= 2000)
                    {
                        s_u32LastBostSendTime = GulSystickCount;
                        LL_GPIO_ResetOutputPin(BOST_PORT, BOST_PIN);
                        s_u32IsNeedBostSend = TRUE;
                    }
                }
                
                iRefreshAsConfig();
            }
        }
        break;
        
        case KEY_CODE_RELEASE:
        {
            if(u8Action == E_KEY_ACTION_SHORT_KEYDOWN)
            {
                bReleaseKeyDownFlag = TRUE;
                
                bSolenoidErrorFlag = FALSE;
                s_u8SolenoidCheckCnt = 0;
            }
            else if(u8Action == E_KEY_ACTION_SHORT_KEYUP)
            {
                u32ReleaseKeyUpTime = GulSystickCount;
                bReleaseKeyDownFlag = FALSE;
                
                bSolenoidErrorFlag = FALSE;
                s_u8SolenoidCheckCnt = 0;
            }
        }
        break;
    }
}

static void iBostMaintThread(void)
{
    if(s_u32IsNeedBostSend == TRUE)
    {
        if(GulSystickCount - s_u32LastBostSendTime >= 100)
        {
            s_u32IsNeedBostSend = FALSE;
            LL_GPIO_SetOutputPin(BOST_PORT, BOST_PIN);
        }
    }
}

static uint32 s_u32LastStartTime = 0;
static void iSolenoidMainThread(void)
{
    static uint32 s_u32LastReleaseTime = 0;
    static uint8 s_u8ReleaseFlag = 0;

    if(s_bIsChargingFlag == TRUE)
    {
        return;
    }
    
    if(s_sScConfig.ePowerState == E_PWOER_STATE_STOP_WORKING)
    {
        return;
    }
    
    if(s_sScConfig.eSwitch == E_SWITCH_STATUS_OFF)
    {
        return;
    }    
    
    if(s_sScConfig.eSuctionStatus != E_SUCTION_STATUS_ON)
    {
        return;
    }
    
    if(s_sScConfig.eSuctionStrength == E_SUCTION_STRENGTH_LEVEL1)
    {
        return;
    }
    
    if(GulSystickCount - s_u32LastReleaseTime >= s_u32ReleaseInterval)
    {
        s_u32LastReleaseTime = GulSystickCount;      //记录一个放气周期开始的时间
        
        s_u32LastStartTime = GulSystickCount;
        s_u8ReleaseFlag = 1;
        
        GLHSOLENOID_Set(E_SOLENOID_STATUS_OPEN);     //开启放气
    }
    
    if(s_u8ReleaseFlag == 1)
    {
        if(GulSystickCount - s_u32LastStartTime >= 100)
        {
            s_u8ReleaseFlag = 0;
            GLHSOLENOID_Set(E_SOLENOID_STATUS_CLOSE); //停止放气
        }
    }
}

static void iBatteryVoltageCheckMainThread(void)
{
    static uint32 s_u32LastBatteryVoltageCheckTime = 0;
    uint8 i = 0, j = 0;
    uint32 u32AdValueTotal = 0;
    uint16 u16Advalue = 0, u16AdvalueMax = 0, u16AdvalueMin = 0xffff;
    float fVoltageTemp = 0;
    float fVoltageTotal = 0, fVoltageTotalMax = 0, fVoltageTotalMin = 100000.0;
    uint32 u32WaitStartTime = 0;
    
    if(BatVoatageInitFlag == FALSE)    //需要初始化一次，连续采集多次，填满滑动滤波数组
    {
        BatVoatageInitFlag = TRUE;
        s_u32LastBatteryVoltageCheckTime = 0xFFFFD8F0;//0 - 10000;
        
        for(j=0; j<BATTERY_VOLTAGE_SLIDE_FILTERING_BUFF_LEN; j++)
        {
            //去极值求平均获取当前电压
            u32AdValueTotal = 0;
            for(i=0; i<18; i++)
            {
                u16Advalue = GLHADC_Get(BATTERY_VOLTAGE_ADC_CH);
            
                u32AdValueTotal += u16Advalue;
            
                if(u16AdvalueMin > u16Advalue)
                {
                    u16AdvalueMin = u16Advalue;
                }
            
                if(u16AdvalueMax < u16Advalue)
                {
                    u16AdvalueMax = u16Advalue;
                }
            }
            fVoltageTemp = 4.2;//((u32AdValueTotal - u16AdvalueMin - u16AdvalueMax) >> 4) * 1.5 * 4 / 4095;       
        
            memcpy(&fBatteryVoltageSlideFilteringBuff[0], &fBatteryVoltageSlideFilteringBuff[1], (BATTERY_VOLTAGE_SLIDE_FILTERING_BUFF_LEN - 1)*sizeof(float));
            fBatteryVoltageSlideFilteringBuff[BATTERY_VOLTAGE_SLIDE_FILTERING_BUFF_LEN - 1] = fVoltageTemp;        
        }
    }
    
    if(GulSystickCount - s_u32LastBatteryVoltageCheckTime > 1000)
    {
        s_u32LastBatteryVoltageCheckTime = GulSystickCount;
        
        //去极值求平均获取当前电压
        u32AdValueTotal = 0;
        for(i=0; i<18; i++)
        {
            u16Advalue = GLHADC_Get(BATTERY_VOLTAGE_ADC_CH);
            
            u32AdValueTotal += u16Advalue;
            
            if(u16AdvalueMin > u16Advalue)
            {
                u16AdvalueMin = u16Advalue;
            }
            
            if(u16AdvalueMax < u16Advalue)
            {
                u16AdvalueMax = u16Advalue;
            }
        }
        fVoltageTemp = ((u32AdValueTotal - u16AdvalueMin - u16AdvalueMax) >> 4) * 1.5 * 4 / 4095;       
        
        memcpy(&fBatteryVoltageSlideFilteringBuff[0], &fBatteryVoltageSlideFilteringBuff[1], (BATTERY_VOLTAGE_SLIDE_FILTERING_BUFF_LEN - 1)*sizeof(float));
        fBatteryVoltageSlideFilteringBuff[BATTERY_VOLTAGE_SLIDE_FILTERING_BUFF_LEN - 1] = fVoltageTemp;
        
        for(i=0; i<BATTERY_VOLTAGE_SLIDE_FILTERING_BUFF_LEN; i++)
        {
            fVoltageTotal += fBatteryVoltageSlideFilteringBuff[i];
            if(fVoltageTotalMin > fBatteryVoltageSlideFilteringBuff[i])
            {
                fVoltageTotalMin = fBatteryVoltageSlideFilteringBuff[i];
            }
            
            if(fVoltageTotalMax < fBatteryVoltageSlideFilteringBuff[i])
            {
                fVoltageTotalMax = fBatteryVoltageSlideFilteringBuff[i];
            }
        }
        fBatVoatage = ((fVoltageTotal - fVoltageTotalMin - fVoltageTotalMax) / (BATTERY_VOLTAGE_SLIDE_FILTERING_BUFF_LEN - 2));
        
        if(s_bIsChargingFlag == TRUE)
        {
            return;
        }
        
        if(s_sScConfig.ePowerState == E_PWOER_STATE_NOMAL)
        {
            if(fBatVoatage < POWER_ALARM_VOLTAGE)
            {
                GLHPWM_Set(PWM_CHANNEL_MOTOR, 0);           //关闭吸气马达
                GLHPWM_Set(PWM_CHANNEL_HOT, 0);             //关闭加热
                GLHLED_TurnOff(LED_ALL);                    //关闭所有指示灯
                GLHSOLENOID_Set(E_SOLENOID_STATUS_CLOSE);   //开启放气
                for(i=0; i<5; i++)
                {
                    GLHLED_TurnOn(LED_STRENGTH_LEVEL1_FLAG | LED_STRENGTH_LEVEL2_FLAG | LED_STRENGTH_LEVEL3_FLAG | LED_HOT_LEVEL1_FLAG | LED_HOT_LEVEL2_FLAG | LED_HOT_LEVEL3_FLAG | ORANGE_LED_BAT_FLAG);
                    u32WaitStartTime = GulSystickCount;
                    while(GulSystickCount - u32WaitStartTime < 300)
                    {
            
                    }
            
                    GLHLED_TurnOff(LED_STRENGTH_LEVEL1_FLAG | LED_STRENGTH_LEVEL2_FLAG | LED_STRENGTH_LEVEL3_FLAG | LED_HOT_LEVEL1_FLAG | LED_HOT_LEVEL2_FLAG | LED_HOT_LEVEL3_FLAG | ORANGE_LED_BAT_FLAG);
                    u32WaitStartTime = GulSystickCount;
                    while(GulSystickCount - u32WaitStartTime < 300)
                    {
            
                    }
                }
                s_sScConfig.ePowerState = E_PWOER_STATE_ALARM;
                
                s_u32VoltageUpStartTime = GulSystickCount;
                s_u32SuctionStrengthAdjustTime = GulSystickCount;
                if(GulSystickCount - s_u32LastBostSendTime >= 1200)
                {
                    s_u32LastBostSendTime = GulSystickCount;
                    LL_GPIO_ResetOutputPin(BOST_PORT, BOST_PIN);
                    s_u32IsNeedBostSend = TRUE;
                }
                iRefreshAsConfig();
            }
            else if(fBatVoatage < POWER_OFF_VOLTAGE)
            {
                s_sScConfig.ePowerState = E_PWOER_STATE_STOP_WORKING;
                iRefreshAsConfig();
            }
        }
        else if(s_sScConfig.ePowerState == E_PWOER_STATE_ALARM)
        {
            if(fBatVoatage < POWER_OFF_VOLTAGE)
            {
                s_sScConfig.ePowerState = E_PWOER_STATE_STOP_WORKING;
                iRefreshAsConfig();
            }
        }
    }
}
float Ntc_Voltage;
static float iGetTemprature(void)
{
    uint16 u16AdValue;
  
    uint32 u32AdValueTotal = 0;
    uint16 u16AdValueMax = 0;
    uint16 u16AdValueMin = 0xffff;
    uint8 i = 0;
    float fNtcRes = 0;
    float fTemp = 0;
    
    for(i=0; i<34; i++)
    {
        u16AdValue = GLHADC_Get(TEMPRATURE_ADC_CH);

        u32AdValueTotal += u16AdValue;
        
        if(u16AdValueMin > u16AdValue)
        {
            u16AdValueMin = u16AdValue;
        }
        
        if(u16AdValueMax < u16AdValue)
        {
            u16AdValueMax = u16AdValue;
        }
    }
    
    u16AdValue = (u32AdValueTotal - u16AdValueMin - u16AdValueMax) >> 5;
    Ntc_Voltage = (float)u16AdValue / 4095.0 * 1.5;
	
    fNtcRes = 200.0 * Ntc_Voltage / (3.3 - Ntc_Voltage);   //电阻单位为KΩ 
	
    if(u16AdValue >= 4000)       //NTC开路
    {
        return NTC_OPEN_TEMPRATURE;
    }
    
    if(fNtcRes > 300)
    {
        return NTC_OPEN_TEMPRATURE;
    }
    else if(fNtcRes < 0.1)
    {
        return NTC_SHORT_TEMPRATURE;
    }
    else if(fNtcRes >= afNtcRes[0])
    {
        fTemp = 0;
    }
    else if(fNtcRes <= afNtcRes[125])
    {
        fTemp = 125;
    }
    else
    {
        for(i=0; i<126; i++)
        {
            if(fNtcRes >= afNtcRes[i])
            {
                fTemp = i - ((afNtcRes[i] - fNtcRes) / (afNtcRes[i] - afNtcRes[i - 1]));
                break;
            }
        }
    }
    
    return fTemp;
}


static void iTempratureMainThread(void)
{
    static uint32 s_u32LastCheckTempraturTime = 0;
    float fTemprature = 0;

    if(s_bIsChargingFlag == TRUE)
    {
        return;
    }
    
    if(s_sScConfig.ePowerState == E_PWOER_STATE_STOP_WORKING)
    {
        return;
    }
    
    if(s_sScConfig.eSwitch == E_SWITCH_STATUS_OFF)
    {
        return;
    }
    
    if(s_sScConfig.eHotStatus == E_HOT_STATUS_OFF)     //加热功能关闭
    {
        return;
    }
    
    if(GulSystickCount - s_u32LastCheckTempraturTime >= 100)
    {
        s_u32LastCheckTempraturTime = GulSystickCount;
        
        fTemprature = iGetTemprature();
        if((fTemprature == NTC_OPEN_TEMPRATURE) || (fTemprature == NTC_SHORT_TEMPRATURE))    
        {
            GLHPWM_Set(PWM_CHANNEL_HOT, 0);                   //关闭加热
            return;
        }
        
        if(s_sScConfig.eHotLevl == E_HOT_LEVEL_LEVEL1)      //实测40±2
        {
            if(s_bIsNeedFullHotFlag1 == TRUE)
            {
                if((fTemprature >= HOT_LEVEL1_STOP_TEMPRATURE + 8) || (GulSystickCount - s_u32NeedFullStartTime > 60000))   //150000  //100s - 39
                {
                    s_bIsNeedFullHotFlag1 = FALSE;
                }
            }
            
            if(s_bIsNeedFullHotFlag1 == TRUE)
            {
                GLHPWM_Set(PWM_CHANNEL_HOT, GLHPWM_MAX);      //开启加热
            }
            else
            {
                if(fTemprature <= HOT_LEVEL1_FULL_TEMPRATURE)
                {
                    GLHPWM_Set(PWM_CHANNEL_HOT, GLHPWM_MAX);      //开启加热
                }
                else if(fTemprature >= HOT_LEVEL1_STOP_TEMPRATURE)
                {
                    GLHPWM_Set(PWM_CHANNEL_HOT, 0);               //关闭加热                
                }
                else
                {
                    GLHPWM_Set(PWM_CHANNEL_HOT, ((float)GLHPWM_MAX)*((float)HOT_LEVEL1_STOP_TEMPRATURE - (float)fTemprature) / ((float)HOT_LEVEL1_STOP_TEMPRATURE - (float)HOT_LEVEL1_FULL_TEMPRATURE));      //开启加热                
                }
            }
        }
        else if(s_sScConfig.eHotLevl == E_HOT_LEVEL_LEVEL2)      //实测40±2
        {
            if(s_bIsNeedFullHotFlag2 == TRUE)
            {
                if((fTemprature >= HOT_LEVEL2_STOP_TEMPRATURE + 10) || (GulSystickCount - s_u32NeedFullStartTime > 90000))   //150000  //100s - 39
                {
                    s_bIsNeedFullHotFlag2 = FALSE;
                }
            }
            
            if(s_bIsNeedFullHotFlag2 == TRUE)
            {
                GLHPWM_Set(PWM_CHANNEL_HOT, GLHPWM_MAX);      //开启加热
            }
            else
            {
                if(fTemprature <= HOT_LEVEL2_FULL_TEMPRATURE)
                {
                    GLHPWM_Set(PWM_CHANNEL_HOT, GLHPWM_MAX);      //开启加热
                }
                else if(fTemprature >= HOT_LEVEL2_STOP_TEMPRATURE)
                {
                    GLHPWM_Set(PWM_CHANNEL_HOT, 0);               //关闭加热                
                }
                else
                {
                    GLHPWM_Set(PWM_CHANNEL_HOT, ((float)GLHPWM_MAX)*((float)HOT_LEVEL2_STOP_TEMPRATURE - (float)fTemprature) / ((float)HOT_LEVEL2_STOP_TEMPRATURE - (float)HOT_LEVEL2_FULL_TEMPRATURE));      //开启加热                
                }
            }
        }
        else if(s_sScConfig.eHotLevl == E_HOT_LEVEL_LEVEL3)      
        {
            if(s_bIsNeedFullHotFlag3 == TRUE)
            {
                if((fTemprature >= HOT_LEVEL3_STOP_TEMPRATURE + 10) || (GulSystickCount - s_u32NeedFullStartTime > 120000))     
                {
                    s_bIsNeedFullHotFlag3 = FALSE;
                }
            }
            
            if(s_bIsNeedFullHotFlag3 == TRUE)
            {
                GLHPWM_Set(PWM_CHANNEL_HOT, GLHPWM_MAX);      //开启加热
            }
            else
            {
                if(fTemprature <= HOT_LEVEL3_FULL_TEMPRATURE)
                {
                    GLHPWM_Set(PWM_CHANNEL_HOT, GLHPWM_MAX);      //开启加热
                }
                else if(fTemprature >= HOT_LEVEL3_STOP_TEMPRATURE)
                {
                    GLHPWM_Set(PWM_CHANNEL_HOT, 0);               //关闭加热                
                }
                else
                {
                    GLHPWM_Set(PWM_CHANNEL_HOT, ((float)GLHPWM_MAX)*((float)HOT_LEVEL3_STOP_TEMPRATURE - (float)fTemprature) / ((float)HOT_LEVEL3_STOP_TEMPRATURE - (float)HOT_LEVEL3_FULL_TEMPRATURE));      //开启加热                
                }
            }
        }
    }
}

static void iChargeIoInit(void)
{
    LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOB);
    LL_GPIO_SetPinMode(CHARGE_CHECK_PORT, CHARGE_CHECK_PIN, LL_GPIO_MODE_INPUT);
    LL_GPIO_SetPinPull(CHARGE_CHECK_PORT, CHARGE_CHECK_PIN, LL_GPIO_PULL_UP);
}

static void iSolenoidCheckPinInit(void)
{
    LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOC);
    LL_GPIO_SetPinMode(SOLENOID_CHECK_PORT, SOLENOID_CHECK_PIN, LL_GPIO_MODE_INPUT);
    LL_GPIO_SetPinPull(SOLENOID_CHECK_PORT, SOLENOID_CHECK_PIN, LL_GPIO_PULL_DOWN);
}

static void iMotorCheckPinInit(void)
{
    LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOC);
    LL_GPIO_SetPinMode(MOTOR_CHECK_PORT, MOTOR_CHECK_PIN, LL_GPIO_MODE_INPUT);
    LL_GPIO_SetPinPull(MOTOR_CHECK_PORT, MOTOR_CHECK_PIN, LL_GPIO_PULL_DOWN);
}

static void iBostPinInit(void)
{
    LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOB);
    LL_GPIO_SetPinMode(BOST_PORT, BOST_PIN, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetOutputPin(BOST_PORT, BOST_PIN);
}

static void iHotCheckPinInit(void)
{
    LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOA);
    LL_GPIO_SetPinMode(HOT_CHECK_PORT, HOT_CHECK_PIN, LL_GPIO_MODE_INPUT);
    LL_GPIO_SetPinPull(HOT_CHECK_PORT, HOT_CHECK_PIN, LL_GPIO_PULL_DOWN);
}

static void iChargeCheckMainThread(void)
{
    static uint32 s_u32LastCheckTime = 0;
    static uint8 s_u8CheckCnt = 0;
    static uint32 s_u32ChargeLastFlashTime = 0;
    static uint32 s_u32LastChargeStatusPinCheckTime = 0;
    static BOOL s_bChargeFlashFlag = FALSE;
    static uint8 s_u8StatusPinCnt = 0;
    static uint32 s_u32LastStableLowLevelTime = 0;        //充电状态脚上一次检测到稳定低电平的时间，说明灯灭了，充电没有充满
    
    if(s_bIsChargingFlag == FALSE)          //非充电状态
    {
        if(GulSystickCount - s_u32LastCheckTime >= 10)
        {
            s_u32LastCheckTime = GulSystickCount;

            if(LL_GPIO_IsInputPinSet(CHARGE_CHECK_PORT, CHARGE_CHECK_PIN) != 0)
            {
                s_u8CheckCnt++;
                if(s_u8CheckCnt > 200)
                {
                    s_u8CheckCnt = 200;
                }
                
                if(s_u8CheckCnt == 5)
                {
                    s_sScConfig.ePowerState = E_PWOER_STATE_NOMAL;
                    
                    s_bIsChargingFlag = TRUE;      //IO口被拉高，进入充电状态
                    s_u8CheckCnt = 0;
                    
                    s_bIsChargeFullFlag = FALSE;                 //刚进入充电状态，默认没有充满。
                    
                    GLHPWM_Set(PWM_CHANNEL_MOTOR, 0);           //关闭吸气马达
                    GLHPWM_Set(PWM_CHANNEL_HOT, 0);             //关闭加热
                    GLHLED_TurnOff(LED_ALL);                    //关闭所有指示灯
                    GLHSOLENOID_Set(E_SOLENOID_STATUS_CLOSE);   //开启放气
                }
            }
            else
            {
                s_u8CheckCnt = 0;
            }
        }
    }
    else                                 //充电状态
    {
        if(GulSystickCount - s_u32LastCheckTime >= 10)
        {
            s_u32LastCheckTime = GulSystickCount;

            
            if(LL_GPIO_IsInputPinSet(CHARGE_CHECK_PORT, CHARGE_CHECK_PIN) == 0)
            {
                s_u8CheckCnt++;
                if(s_u8CheckCnt > 200)
                {
                    s_u8CheckCnt = 200;
                }
                
                if(s_u8CheckCnt == 5)
                {
                    s_bIsChargingFlag = FALSE;      //IO口被拉低，进入非充电状态
                    s_u8CheckCnt = 0;
                    
                    s_sScConfig.eSwitch = E_SWITCH_STATUS_OFF;
                    iRefreshAsConfig();
                }
            }
            else
            {
                s_u8CheckCnt = 0;
            }
        }    
    }
    
    if(s_bIsChargingFlag == TRUE)    //正在充电
    {   
        if(GulSystickCount - s_u32LastChargeStatusPinCheckTime >= 10)
        {
            s_u32LastChargeStatusPinCheckTime = GulSystickCount;
            
            LL_GPIO_SetPinMode(CHARGE_STATUS_PORT, CHARGE_STATUS_PIN, LL_GPIO_MODE_INPUT);
            LL_GPIO_SetPinPull(CHARGE_STATUS_PORT, CHARGE_STATUS_PIN, LL_GPIO_PULL_NO);              
            if(LL_GPIO_IsInputPinSet(CHARGE_STATUS_PORT, CHARGE_STATUS_PIN) == 0)
            {
                s_u8StatusPinCnt++;
                if(s_u8StatusPinCnt > 250)
                {
                    s_u8StatusPinCnt = 250;
                }
                
                if(s_u8StatusPinCnt == 50)
                {
                    s_bIsChargeFullFlag = FALSE;
                    s_u32LastStableLowLevelTime = GulSystickCount;
                }
            }
            else
            {
                s_u8StatusPinCnt = 0;
            }
            LL_GPIO_SetPinMode(CHARGE_STATUS_PORT, CHARGE_STATUS_PIN, LL_GPIO_MODE_OUTPUT);
        }
        
        if(s_bIsChargeFullFlag == FALSE)
        {
            if(GulSystickCount - s_u32LastStableLowLevelTime >= 3000)     //超过3秒status引脚都没有检测到稳定的低电平，说明电已经冲满了。
            {
                s_bIsChargeFullFlag = TRUE;
                GLHLED_TurnOn(WHITE_LED_BAT_FLAG);
            }
        }
        
        if(s_bIsChargeFullFlag == FALSE)
        {
            if(GulSystickCount - s_u32ChargeLastFlashTime >= 500)
            {
                s_u32ChargeLastFlashTime = GulSystickCount;
                
                s_bChargeFlashFlag = !s_bChargeFlashFlag;
                if(s_bChargeFlashFlag)
                {
                    GLHLED_TurnOn(WHITE_LED_BAT_FLAG);
                }
                else
                {
                    GLHLED_TurnOff(WHITE_LED_BAT_FLAG);
                }
            }
        }
    }
}

volatile uint8 u8StopMOdeTestFlag = 0;
static void iEnterSleepMode(void)
{
    #if 1
    LL_GPIO_InitTypeDef GPIO_InitStruct;
    LL_EXTI_InitTypeDef EXTI_InitStruct;
    
//    LL_USART_DeInit(USART1);
    
    /* 使能GPIOB */
    LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOB);
    /* 配置PB5输入模式 */
    GPIO_InitStruct.Pin = LL_GPIO_PIN_4;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    /* 选择EXTI0做外部中断输入 */
    LL_EXTI_SetEXTISource(LL_EXTI_CONFIG_PORTB,LL_EXTI_CONFIG_LINE4);
    /* 配置EXTI0为中断、下降沿触发 */
    EXTI_InitStruct.Line = LL_EXTI_LINE_4;
    EXTI_InitStruct.LineCommand = ENABLE;
    EXTI_InitStruct.Mode = LL_EXTI_MODE_IT;
    EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_RISING;
    LL_EXTI_Init(&EXTI_InitStruct);
    /* 使能中断 */
    NVIC_SetPriority(EXTI4_15_IRQn, 0);
    NVIC_EnableIRQ(EXTI4_15_IRQn);
    
    /* 使能GPIOA */
    LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOA);
    /* 配置PA7输入模式 */
    GPIO_InitStruct.Pin = LL_GPIO_PIN_7;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    /* 选择EXTI0做外部中断输入 */
    LL_EXTI_SetEXTISource(LL_EXTI_CONFIG_PORTA, LL_EXTI_CONFIG_LINE7);
    /* 配置EXTI0为中断、下降沿触发 */
    EXTI_InitStruct.Line = LL_EXTI_LINE_7;
    EXTI_InitStruct.LineCommand = ENABLE;
    EXTI_InitStruct.Mode = LL_EXTI_MODE_IT;
    EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_FALLING;
    LL_EXTI_Init(&EXTI_InitStruct);
    /* 使能中断 */
    NVIC_SetPriority(EXTI4_15_IRQn, 0);
    NVIC_EnableIRQ(EXTI4_15_IRQn);

    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_5, LL_GPIO_MODE_OUTPUT);          //1 PA5
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_5, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_5);
    
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_6, LL_GPIO_MODE_ANALOG);           //2 PA6
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_6, LL_GPIO_PULL_NO);
    
//    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_7, LL_GPIO_MODE_INPUT);           //3 PA7
//    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_7, LL_GPIO_PULL_UP);
    
    LL_GPIO_SetPinMode(GPIOC, LL_GPIO_PIN_0, LL_GPIO_MODE_ANALOG);           //4 PC0
    LL_GPIO_SetPinPull(GPIOC, LL_GPIO_PIN_0, LL_GPIO_PULL_NO);
    
    LL_GPIO_SetPinMode(GPIOC, LL_GPIO_PIN_1, LL_GPIO_MODE_ANALOG);           //5 PC1
    LL_GPIO_SetPinPull(GPIOC, LL_GPIO_PIN_1, LL_GPIO_PULL_NO);
    
    LL_GPIO_SetPinMode(GPIOB, LL_GPIO_PIN_7, LL_GPIO_MODE_OUTPUT);          //6 PB7
    LL_GPIO_SetPinOutputType(GPIOB, LL_GPIO_PIN_7, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_7);
    
    LL_GPIO_SetPinMode(GPIOB, LL_GPIO_PIN_6, LL_GPIO_MODE_OUTPUT);          //8 PB6
    LL_GPIO_SetPinOutputType(GPIOB, LL_GPIO_PIN_6, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_7);
    
    LL_GPIO_SetPinMode(GPIOB, LL_GPIO_PIN_5, LL_GPIO_MODE_ANALOG);          //10 PB5
    LL_GPIO_SetPinPull(GPIOB, LL_GPIO_PIN_5, LL_GPIO_PULL_NO);
    
    LL_GPIO_SetPinMode(GPIOB, LL_GPIO_PIN_3, LL_GPIO_MODE_OUTPUT);          //12 PB3
    LL_GPIO_SetPinOutputType(GPIOB, LL_GPIO_PIN_3, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_3);
    
    LL_GPIO_SetPinMode(GPIOB, LL_GPIO_PIN_2, LL_GPIO_MODE_OUTPUT);          //13 PB2
    LL_GPIO_SetPinOutputType(GPIOB, LL_GPIO_PIN_2, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_2);
    
    LL_GPIO_SetPinMode(GPIOB, LL_GPIO_PIN_1, LL_GPIO_MODE_ANALOG);           //14 PB1
    LL_GPIO_SetPinPull(GPIOB, LL_GPIO_PIN_1, LL_GPIO_PULL_NO);
    
    LL_GPIO_SetPinMode(GPIOB, LL_GPIO_PIN_0, LL_GPIO_MODE_OUTPUT);          //15 PB0
    LL_GPIO_SetPinOutputType(GPIOB, LL_GPIO_PIN_0, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_0);
    
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_0, LL_GPIO_MODE_OUTPUT);          //16 PA0
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_0, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_0);
    
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_1, LL_GPIO_MODE_OUTPUT);          //17 PA1
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_1, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_1);
    
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_2, LL_GPIO_MODE_OUTPUT);          //18 PA2
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_2, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_2);
    
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_3, LL_GPIO_MODE_OUTPUT);          //19 PA3
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_3, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_3);
    
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_4, LL_GPIO_MODE_ANALOG);          //20 PA4
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_4, LL_GPIO_PULL_NO);
    
    /* 使能PWR时钟 */
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);
    /* 低功耗运行模式 */
    LL_PWR_SetLprMode(LL_PWR_LPR_MODE_LPR);
    /* SRAM电压跟数字LDO输出一致 */
    LL_PWR_SetStopModeSramVoltCtrl(LL_PWR_SRAM_RETENTION_VOLT_CTRL_LDO);
    /* 进入DeepSleep模式 */
    LL_LPM_EnableDeepSleep();
    /* 等待中断指令 */
    __WFI();
    LL_LPM_EnableSleep();

    s_u32WakeUpTime = GulSystickCount;
    
    NVIC_DisableIRQ(EXTI4_15_IRQn);

    GLHKEY_Init();
    GLHLED_Init();
    GLHPWM_Init(PWM_CHANNEL_MOTOR | PWM_CHANNEL_HOT);
    GLHADC_Init(TEMPRATURE_ADC_CH | BATTERY_VOLTAGE_ADC_CH);
    GLHSOLENOID_Init();
    iChargeIoInit();
    iSolenoidCheckPinInit();
    iMotorCheckPinInit();
    iBostPinInit();
    iHotCheckPinInit();
    #endif
}

void EXTI4_15_IRQHandler(void)
{
    u8StopMOdeTestFlag = 1;
    
    /* 处理EXTI中断请求 */
    if(LL_EXTI_IsActiveFlag(LL_EXTI_LINE_4))
    {
        LL_EXTI_ClearFlag(LL_EXTI_LINE_4);
    }
    
    /* 处理EXTI中断请求 */
    if(LL_EXTI_IsActiveFlag(LL_EXTI_LINE_7))
    {
        LL_EXTI_ClearFlag(LL_EXTI_LINE_7);
    }
}

static void iPowerOffNoSleepCheckMainThread(void)
{
    if(LL_GPIO_IsInputPinSet(CHARGE_CHECK_PORT, CHARGE_CHECK_PIN) != 0)    //正在充电
    {
        return;
    }
    
    if(s_bIsChargingFlag == TRUE)
    {
        return;
    }
    
    if(s_sScConfig.eSwitch == E_SWITCH_STATUS_OFF)
    {
        if(GulSystickCount - s_u32WakeUpTime >= 2000)
        {
            if(GulSystickCount - s_u32PowerOffStartTime >= 3000)
            {
                iEnterSleepMode();
            }
        }
    }
}


//检查电机和电磁阀是否有正常工作
static void MotorSolenoidCheckMainThread(void)
{  
    uint8 i = 0;
    uint32 u32WaitStartTime = 0;
    static uint8 MotorCheckCnt = 0, HotCheckCnt = 0;
    static uint32 s_u32LastChekHotTime = 0;

    if(s_bIsChargingFlag == TRUE)
    {
        s_u8SolenoidCheckCnt = 0;
        return;
    } 
    
    if(s_sScConfig.eSwitch != E_SWITCH_STATUS_ON)
    {
        s_u8SolenoidCheckCnt = 0;
        return;
    }
    
    if(s_sScConfig.eSuctionStatus == E_SUCTION_STATUS_ON)
    {
        if(GulSystickCount - s_u32VoltageUpStartTime < 1000)
        {
            s_u8SolenoidCheckCnt = 0;
            return;
        }
        
        
        //电磁阀异常检测
        if(bSolenoidErrorFlag == FALSE)
        {
            if(bReleaseKeyDownFlag == FALSE)
            {
                if(GulSystickCount - u32ReleaseKeyUpTime >= 500)
                {
                    if((SOLENOID_CTR_PORT->ODR & SOLENOID_CTR_PIN) == 0)  //控制电磁阀的引脚输出低电平
                    {
                        if(((SOLENOID_CHECK_PORT->IDR) & SOLENOID_CHECK_PIN) == 0)
                        {
                            s_u8SolenoidCheckCnt++;
                            if(s_u8SolenoidCheckCnt >= 200)
                            {
                                s_u8SolenoidCheckCnt = 0;
                                bSolenoidErrorFlag = TRUE;
                                u32SolenoidErrorStartTime = GulSystickCount;
                            }
            
                            if(s_u8SolenoidCheckCnt > 250)
                            {
                                s_u8SolenoidCheckCnt = 250;
                            }
                        }
                        else
                        {
                            s_u8SolenoidCheckCnt = 0;
                        }
                    }
                }
                else
                {
                    s_u8SolenoidCheckCnt = 0;
                }
            }
            else
            {
                s_u8SolenoidCheckCnt = 0;
            }
        }
        else
        {
            s_u8SolenoidCheckCnt = 0;
            if(GulSystickCount - u32SolenoidErrorStartTime >= 200)
            {
                bSolenoidErrorFlag = FALSE;
                goto POWER_OFF;
            }
        }
        
        //电机异常加测
        if(GulSystickCount - s_u32SuctionStrengthAdjustTime >= 100)
        {
            if(((TIM1->CNT) > (TIM1->CCR3)) && ((TIM1->CNT) < 195))
            {
                if(((MOTOR_CHECK_PORT->IDR) & MOTOR_CHECK_PIN) == 0)
                {
                    MotorCheckCnt++;
                    if(MotorCheckCnt >= 5)
                    {
                        MotorCheckCnt = 0;
                        goto POWER_OFF;
                    }
            
                    if(MotorCheckCnt > 200)
                    {
                        MotorCheckCnt = 200;
                    }
                }
                else
                {
                    MotorCheckCnt = 0;
                }
            }
        }
    }
    
    //加热异常测
    if(s_sScConfig.eHotStatus == E_HOT_STATUS_ON)
    {
        if((TIM1->CCR1) == GLHPWM_MAX)     //  满功率加热，MOS管一直打开，没办法直接检测。需要人为输出低电平才能检测
        {
            if(GulSystickCount - s_u32LastChekHotTime >= 1000)
            {
                s_u32LastChekHotTime = GulSystickCount;
            
                LL_GPIO_ResetOutputPin(HOT_PORT, HOT_PIN);
                LL_GPIO_SetPinMode(HOT_PORT, HOT_PIN, LL_GPIO_MODE_OUTPUT);
                __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                if(((HOT_CHECK_PORT->IDR) & HOT_CHECK_PIN) == 0)
                {
                    __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                    __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                    __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                    __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                    __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                    __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                    __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                    __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                    __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                    __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                    __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                    __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
                    if(((HOT_CHECK_PORT->IDR) & HOT_CHECK_PIN) == 0)
                    {
                        LL_GPIO_SetPinMode(HOT_PORT, HOT_PIN, LL_GPIO_MODE_ALTERNATE);
                        goto POWER_OFF;
                    }
                }
                LL_GPIO_SetPinMode(HOT_PORT, HOT_PIN, LL_GPIO_MODE_ALTERNATE);
            }
        }
        else
        {
            s_u32LastChekHotTime = GulSystickCount;
            if(((TIM1->CNT) > (TIM1->CCR1)) && ((TIM1->CNT) < 195))
            {
                if(((HOT_CHECK_PORT->IDR) & HOT_CHECK_PIN) == 0)
                {
                    HotCheckCnt++;
                    if(HotCheckCnt >= 5)
                    {
                        HotCheckCnt = 0;
                        goto POWER_OFF;
                    }
            
                    if(HotCheckCnt > 200)
                    {
                        HotCheckCnt = 200;
                    }
                }
                else
                {
                    HotCheckCnt = 0;
                }
            }
        }
    }
    
    return;
    
    POWER_OFF:    
    GLHPWM_Set(PWM_CHANNEL_MOTOR, 0);           //关闭吸气马达
    GLHPWM_Set(PWM_CHANNEL_HOT, 0);             //关闭加热
    GLHLED_TurnOff(LED_ALL);                    //关闭所有指示灯
    GLHSOLENOID_Set(E_SOLENOID_STATUS_CLOSE);   //开启放气
    for(i=0; i<2; i++)
    {
        GLHLED_TurnOn(LED_STRENGTH_LEVEL1_FLAG | LED_STRENGTH_LEVEL2_FLAG | LED_STRENGTH_LEVEL3_FLAG | LED_HOT_LEVEL1_FLAG | LED_HOT_LEVEL2_FLAG | LED_HOT_LEVEL3_FLAG | ORANGE_LED_BAT_FLAG);
        u32WaitStartTime = GulSystickCount;
        while(GulSystickCount - u32WaitStartTime < 300)
        {
            
        }
            
        GLHLED_TurnOff(LED_STRENGTH_LEVEL1_FLAG | LED_STRENGTH_LEVEL2_FLAG | LED_STRENGTH_LEVEL3_FLAG | LED_HOT_LEVEL1_FLAG | LED_HOT_LEVEL2_FLAG | LED_HOT_LEVEL3_FLAG | ORANGE_LED_BAT_FLAG);
        u32WaitStartTime = GulSystickCount;
        while(GulSystickCount - u32WaitStartTime < 300)
        {
            
        }
    }
    
    s_sScConfig.eSwitch = E_SWITCH_STATUS_OFF;
    GLHPWM_Set(PWM_CHANNEL_MOTOR, 0);           //关闭吸气马达
    GLHPWM_Set(PWM_CHANNEL_HOT, 0);             //关闭加热
    GLHLED_TurnOff(LED_ALL);                    //关闭所有指示灯
    GLHSOLENOID_Set(E_SOLENOID_STATUS_CLOSE);   //开启放气
}
