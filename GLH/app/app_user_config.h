/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称: app_user_config.h
* 文件标识：
* 摘 要：
*   应用层通用配置文件
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2021年6月21日
*/
#ifdef  __cplusplus
extern "C" {
#endif


#define SUPPORT_WD          //是否支持看门狗
#define SUPPORT_UART        //是否支持串口
#define SUPPORT_FLASH       //是否支持FLASH
#define SUPPORT_KEY
#define SUPPORT_CHIP_ADDR
//#define SUPPORT_RF433
//#define SUPPORT_DM          //支持DM模块
#define SUPPORT_OVER_CURRENT_PROTECT

#define SUPPORT_PWM
#define PWM_CHANNEL_R       E_PWM_CH0
#define PWM_CHANNEL_G       E_PWM_CH1
#define PWM_CHANNEL_B       E_PWM_CH2
#define PWM_CHANNEL_W       E_PWM_CH3

#define PWM_CHANNEL_MOTOR   E_PWM_CH3
#define PWM_CHANNEL_HOT     E_PWM_CH1

#define TEMPRATURE_ADC_CH               GLHADC_CHANNEL_0
#define BATTERY_VOLTAGE_ADC_CH          GLHADC_CHANNEL_2

#define CODE                const
	
	
#define GLH_COLOUR_MAX       255
	
#define XDATA   
	
#ifdef  __cplusplus
}
#endif

