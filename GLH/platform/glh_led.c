#include "main.h"
#include "glh_led.h"
#include "glh_sys_tick.h"

#define LEDH1_PORT      GPIOB
#define LEDH1_PIN       LL_GPIO_PIN_6
#define LEDH1_HIGH()    LL_GPIO_SetOutputPin(LEDH1_PORT, LEDH1_PIN)
#define LEDH1_LOW()     LL_GPIO_ResetOutputPin(LEDH1_PORT, LEDH1_PIN)

#define LEDH2_PORT      GPIOB
#define LEDH2_PIN       LL_GPIO_PIN_7
#define LEDH2_HIGH()    LL_GPIO_SetOutputPin(LEDH2_PORT, LEDH2_PIN)
#define LEDH2_LOW()     LL_GPIO_ResetOutputPin(LEDH2_PORT, LEDH2_PIN)

#define LEDH3_PORT      GPIOA
#define LEDH3_PIN       LL_GPIO_PIN_0
#define LEDH3_HIGH()    LL_GPIO_SetOutputPin(LEDH3_PORT, LEDH3_PIN)
#define LEDH3_LOW()     LL_GPIO_ResetOutputPin(LEDH3_PORT, LEDH3_PIN)

#define LEDL1_PORT      GPIOA
#define LEDL1_PIN       LL_GPIO_PIN_2
#define LEDL1_HIGH()    LL_GPIO_SetOutputPin(LEDL1_PORT, LEDL1_PIN)
#define LEDL1_LOW()     LL_GPIO_ResetOutputPin(LEDL1_PORT, LEDL1_PIN)

#define LEDL2_PORT      GPIOB
#define LEDL2_PIN       LL_GPIO_PIN_0
#define LEDL2_HIGH()    LL_GPIO_SetOutputPin(LEDL2_PORT, LEDL2_PIN)
#define LEDL2_LOW()     LL_GPIO_ResetOutputPin(LEDL2_PORT, LEDL2_PIN)

#define LEDL3_PORT      GPIOA
#define LEDL3_PIN       LL_GPIO_PIN_1
#define LEDL3_HIGH()    LL_GPIO_SetOutputPin(LEDL3_PORT, LEDL3_PIN)
#define LEDL3_LOW()     LL_GPIO_ResetOutputPin(LEDL3_PORT, LEDL3_PIN)

#define LEDL_OUTSIDE_PORT      GPIOA
#define LEDL_OUTSIDE_PIN       LL_GPIO_PIN_3
#define LEDL_OUTSIDE_HIGH()    LL_GPIO_SetOutputPin(LEDL_OUTSIDE_PORT, LEDL_OUTSIDE_PIN)
#define LEDL_OUTSIDE_LOW()     LL_GPIO_ResetOutputPin(LEDL_OUTSIDE_PORT, LEDL_OUTSIDE_PIN)

uint16 u16LedState = 0;

static void iTimerInit(void)
{
    /*配置TIM14*/
    LL_TIM_InitTypeDef TIM14CountInit = {0};
  
    TIM14CountInit.ClockDivision       = LL_TIM_CLOCKDIVISION_DIV1;     /* 不分频             */
    TIM14CountInit.CounterMode         = LL_TIM_COUNTERMODE_UP;         /* 计数模式：向上计数 */
    TIM14CountInit.Prescaler           = 80-1;                          /* 时钟预分频：80     */
    TIM14CountInit.Autoreload          = 1000-1;                        /* 自动重装载值：1000 */
    TIM14CountInit.RepetitionCounter   = 0;                             /* 重复计数值：0      */

    /*使能TIM14时钟*/
    LL_APB1_GRP2_EnableClock(RCC_APBENR2_TIM14EN);
    
    /*初始化TIM14*/
    LL_TIM_Init(TIM14,&TIM14CountInit);

    /*清除更新标志位*/
    LL_TIM_ClearFlag_UPDATE(TIM14);
    
    /*使能UPDATE中断*/
    LL_TIM_EnableIT_UPDATE(TIM14);
  
    /*使能TIM1计数器*/
    LL_TIM_EnableCounter(TIM14);
  
    NVIC_EnableIRQ(TIM14_IRQn);
    NVIC_SetPriority(TIM14_IRQn, 3);
}

static void iGpioInit(void)
{
    LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOA);
    LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOB);
    
    LL_GPIO_SetPinMode(LEDH1_PORT, LEDH1_PIN, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(LEDH1_PORT, LEDH1_PIN, LL_GPIO_OUTPUT_PUSHPULL);
    LEDH1_LOW();
    
    LL_GPIO_SetPinMode(LEDH2_PORT, LEDH2_PIN, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(LEDH2_PORT, LEDH2_PIN, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_ResetOutputPin(LEDH2_PORT, LEDH2_PIN);
    LEDH2_LOW();
    
    LL_GPIO_SetPinMode(LEDH3_PORT, LEDH3_PIN, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(LEDH3_PORT, LEDH3_PIN, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_ResetOutputPin(LEDH3_PORT, LEDH3_PIN);
    LEDH3_LOW();
    
    LL_GPIO_SetPinMode(LEDL1_PORT, LEDL1_PIN, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(LEDL1_PORT, LEDL1_PIN, LL_GPIO_OUTPUT_PUSHPULL);
    LEDL1_HIGH();
    
    LL_GPIO_SetPinMode(LEDL2_PORT, LEDL2_PIN, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(LEDL2_PORT, LEDL2_PIN, LL_GPIO_OUTPUT_PUSHPULL);
    LEDL2_HIGH();
    
    LL_GPIO_SetPinMode(LEDL3_PORT, LEDL3_PIN, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(LEDL3_PORT, LEDL3_PIN, LL_GPIO_OUTPUT_PUSHPULL);
    LEDL3_HIGH();
    
    LL_GPIO_SetPinMode(LEDL_OUTSIDE_PORT, LEDL_OUTSIDE_PIN, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(LEDL_OUTSIDE_PORT, LEDL_OUTSIDE_PIN, LL_GPIO_OUTPUT_PUSHPULL);
    LEDL_OUTSIDE_LOW();
}

void GLHLED_Init(void)
{
    iTimerInit(); 
    iGpioInit();
}

static void iLedScan(void)
{
    static uint8 s_u8Index = 0;
    
    LEDH1_LOW();
    LEDH2_LOW();
    LEDH3_LOW();
    LEDL1_HIGH();
    LEDL2_HIGH();
    LEDL3_HIGH();
    
    s_u8Index++;
    if(s_u8Index > 3)
    {
        s_u8Index = 1;
    }
    switch(s_u8Index)
    {
        case 1:
        {
            if(u16LedState & LED1_FLAG)
            {
                LEDH1_HIGH();
            }

            if(u16LedState & LED4_FLAG)
            {
                LEDH2_HIGH();
            }

            if(u16LedState & LED7_FLAG)
            {
                LEDH3_HIGH();
            }
            
            LEDL1_LOW();
        }
        break;
        
        case 2:
        {
            if(u16LedState & LED2_FLAG)
            {
                LEDH1_HIGH();
            }

            if(u16LedState & LED5_FLAG)
            {
                LEDH2_HIGH();
            }

            if(u16LedState & LED8_FLAG)
            {
                LEDH3_HIGH();
            }
            
            LEDL2_LOW();
        }
        break;
        
        case 3:
        {
            if(u16LedState & LED3_FLAG)
            {
                LEDH1_HIGH();
            }

            if(u16LedState & LED6_FLAG)
            {
                LEDH2_HIGH();
            }

            if(u16LedState & LED9_FLAG)
            {
                LEDH3_HIGH();
            }
            
            LEDL3_LOW();
        }
        break;
        
        default:
            break;
    }
}

void GLHLED_TurnOn(uint16 u16Led)
{
    u16LedState |= u16Led;
    
    if(u16Led & LED1_OUTSIDE_FLAG)
    {
        LEDL_OUTSIDE_HIGH();
    }
}

void GLHLED_TurnOff(uint16 u16Led)
{
    u16LedState &= (~u16Led);
    
    if(u16Led & LED1_OUTSIDE_FLAG)
    {
        LEDL_OUTSIDE_LOW();
    }
}

void TIM14_IRQHandler(void)
{
    LL_TIM_ClearFlag_UPDATE(TIM14);
    iLedScan();
}
