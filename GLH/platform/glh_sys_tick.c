/*
* Copyright (c) 2023, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_sys_tick.c
* 文件标识：
* 摘 要：
*   通过定时器，实现开机到当前的时间
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2023年3月31日
*/
#include "main.h"
#include "glh_sys_tick.h"

volatile uint32 GulSystickCount;  //系统计数器，开机到现在所经历的时间，ms

static void iInitCLK(void)
{
    /* 使能HSI */
    LL_RCC_HSI_Enable();
    while(LL_RCC_HSI_IsReady() != 1)
    {
    }

    /* 设置 AHB 分频*/
    LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);

    /* 配置HSISYS作为系统时钟源 */
    LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_HSISYS);
    while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_HSISYS)
    {
    }

    /* 设置 APB1 分频*/
    LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
    LL_Init1msTick(24000000);

    /* 更新系统时钟全局变量SystemCoreClock(也可以通过调用SystemCoreClockUpdate函数更新) */
    LL_SetSystemCoreClock(24000000);
}

static void iTimerInit(void)
{

}

void GLHST_Init(void)
{
    /* 开SYSCFG和PWR时钟 */
    LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_SYSCFG);
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR); 
    
    iTimerInit();
    iInitCLK();
    SysTick_Config(24000);
    
    while(GulSystickCount < 3000) //上电延时一段时间，防止SWC,SWD,RST引脚被配置成普通IO口后无法下载程序
    {
    
    }
}

void SysTick_Handler(void)
{
    GulSystickCount++;
}
