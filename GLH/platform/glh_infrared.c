/*
* Copyright (c) 2020, 深圳市君同科技有限公司
* All rights reserved.
*
* 文件名称：jt_infrared.c
* 文件标识：
* 摘 要：
*   处理红外事件
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2020年10月9日
*/
#include "jt_infrared.h"
#include "jt_sys_tick.h"

#ifdef SUPPORT_IR

// 77->100us
// 38->50us
#define IR_BASE_TIME       100    // 红外延时基数 100us
#define IR_BASE_TICK        77     // 红外延时基数对应的TICK

uint32 g_u32IrData = 0;  //数据

static BOOL s_bRecEnd = FALSE;   //是否接收完成
BOOL s_bIrStart = FALSE; //是否接收到了引导码
static uint8 s_u8IrBitBum = 0; //接收到的数据位数

static uint8 s_u8IrTick = 0;
static uint32 s_u8LastIOLevel = 0;

#define iReadGpio()  gpio_input_val(JTIR_GPIO)

//函数申明
static void iCheckIr(void);

void JTIR_Init(void)
{
    //初始化io
    gpio_mux_ctl(JTIR_GPIO,0);
    gpio_fun_sel(JTIR_GPIO, GPIO_Dx);
    gpio_fun_inter(JTIR_GPIO,0);
    gpio_direction_input(JTIR_GPIO, GPIO_Mode_Input_Up);//swd  

    s_u8LastIOLevel = iReadGpio();
}

void JTIR_MainThread(void)
{
    do 
    {
        iCheckIr();        
    }while (s_bIrStart || s_u8LastIOLevel==0);

    //处理红外数据
    if (s_bRecEnd)
    {
        JTIR_EventCb();

        s_bRecEnd = FALSE;
    }
}

static void iCheckIr(void)
{
    uint32 io = iReadGpio();

    if (io == s_u8LastIOLevel || io == 0)   //io如果没有变化，或者为下降沿
    {   
        s_u8LastIOLevel = io;
        return;
    }
    
    s_u8LastIOLevel = io;

    //获取高电平时间
    while (iReadGpio())
    {
        JTDL_DelayUs(IR_BASE_TICK);  //约为166us
        s_u8IrTick++;
        if (s_u8IrTick > (5000/IR_BASE_TIME))    // 最大5ms，大于该值就是异常码
        {
            s_bIrStart = FALSE;
            s_u8IrTick = 0;
            return;
        }
    }

    s_u8LastIOLevel = 0;

    if (s_u8IrTick >= (4000/IR_BASE_TIME))   //起始信号4.5
    {
        s_u8IrTick = 0;
        s_bIrStart = TRUE;
        s_u8IrBitBum = 0;
        return;
    }
    else if (s_u8IrTick >= (2250/IR_BASE_TIME))  //重复引导码, 2.25
    {
        s_u8IrTick = 0;
        s_bIrStart = FALSE;
//        s_bRecEnd = TRUE;
        return;
    }

    if (s_bIrStart)
    {
        g_u32IrData >>= 1;
        if (s_u8IrTick >= (100/IR_BASE_TICK) && s_u8IrTick <= (800/IR_BASE_TICK)) // 0 >> 0.56ms的高电平
        {
            g_u32IrData |= 0x00000000;
        }
        else if (s_u8IrTick >= (1200/IR_BASE_TICK) && s_u8IrTick <= (2000/IR_BASE_TICK))  // 1 >>> 1.69ms的高电平
        {
            g_u32IrData |= 0x80000000;
        }
        else
        {
            s_bIrStart = FALSE;
            s_u8IrTick = 0;
            return;
        }

        s_u8IrBitBum++; //收到一位后IR_Num加1
        if(s_u8IrBitBum > 31) //如果IR_Num>31，即等于32（为保证可靠，用大于）时，说明数据已经收完
        {
            s_bRecEnd = TRUE; //数据收完后接收结束标志置位
            s_bIrStart = FALSE; //启动标志清零 //
        }
    }

    s_u8IrTick = 0;
}

BOOL JTIR_GetSignalIsValid(void)
{
    return (!iReadGpio());
}
#endif
