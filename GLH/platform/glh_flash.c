/*
* Copyright (c) 2022, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_flash.c
* 文件标识：
* 摘 要：
*    读写flash模块
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2022年6月22日
*/
#include "glh_flash.h"
#include "main.h"

#ifdef SUPPORT_FLASH

void GLHFLASH_Init(void)
{

}

void GLHFLASH_EraseBlock(uint32 u32Addr, uint16 u16Len)
{
 
}

//uint8 GLHFLASH_ReadByte(uint32 u32Addr)
//{

//}

//void GLHLASH_WriteByte(uint32 u32Addr, uint8 u8Byte)
//{
//    
//}

void GLHFLASH_ReadBytes(uint32 u32Addr, uint8 *pu8Buff, uint16 u16Len)
{

}


void GLHFLASH_WriteBytes(uint32 u32Addr, uint8 *pu8Buff, uint16 u16Len)
{

}

void GLHFLASH_ReadDevId(uint8 *pu8Buff)
{

}

void GLHFLASH_OpitonBytesConfig(void)
{
    FLASH_OBProgramInitTypeDef OBInitCfg;
    
    if(((*((uint32 *)0x1FFF0E80)) & (((uint32)1) << 14)) != 0)    //配置字中NRST引脚已经被配置成了普通引脚
    {
        return;
    }
    
  HAL_FLASH_Unlock();                                 //解锁FLASH
  HAL_FLASH_OB_Unlock();                              //解锁FLASH OPTION
  //==================
  //配置OPTION选项
  //==================
  OBInitCfg.OptionType = OPTIONBYTE_USER;
  OBInitCfg.USERType = OB_USER_BOR_EN | OB_USER_BOR_LEV | OB_USER_IWDG_SW | OB_USER_WWDG_SW | OB_USER_NRST_MODE | OB_USER_nBOOT1;
  //BOR不使能/BOR上升3.2,下降3.1/软件模式看门狗/RST改为GPIO/System memory作为启动区
  OBInitCfg.USERConfig = OB_BOR_DISABLE | OB_BOR_LEVEL_3p1_3p2 | OB_IWDG_SW | OB_WWDG_SW | FLASH_OPTR_NRST_MODE | OB_BOOT1_SYSTEM;
  /* 启动option byte编程 */
  HAL_FLASH_OBProgram(&OBInitCfg);
  HAL_FLASH_Lock();                                   //锁定FLASH
  HAL_FLASH_OB_Lock();                                //锁定FLASH OPTION
  /* 产生一个复位，option byte装载 */
  HAL_FLASH_OB_Launch();
}
#endif
