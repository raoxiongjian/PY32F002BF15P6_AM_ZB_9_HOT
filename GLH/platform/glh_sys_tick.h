/*
* Copyright (c) 2023, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_sys_tick.h
* 文件标识：
* 摘 要：
*   系统时钟模块，实现开机到当前的时间
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2023年3月31日
*/
#ifndef __GLH_SYS_TICK_H__
#define __GLH_SYS_TICK_H__
#include "glh_typedef.h"

extern volatile uint32 GulSystickCount;

/**
    @功能: 系统时钟模块初始化
    @输入:
    @返回:
*/
void GLHST_Init(void);



#endif //__JT_SYS_TICK_H__
