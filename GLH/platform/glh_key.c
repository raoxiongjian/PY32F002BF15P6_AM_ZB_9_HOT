/*
* Copyright (c) 2023, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_key.c
* 文件标识：
* 摘 要：
*   按键处理
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2023年4月3日
*/
#if 0
#include "main.h"
#include "glh_key.h"
#include "glh_sys_tick.h"

#ifdef SUPPORT_KEY

#define GLHKEY_VALID_LEVEL              0
#define GLHKEY_THREAD_INTERVAL          10          // 10ms检测一次
#define GLHKEY_SHORT_KEY_TICK           3
#define GLHKEY_LONG_KEY_TICK            200         //
#define GLHKEY_DOUBLE_INTERVAL          200         // 200ms内双击两次都算双击
#define GLHKEY_CONTINUOUS_TICK          10          //按键长按不松开时，多长时间触发一次

#define KEY1_PIN                        LL_GPIO_PIN_5  
#define KEY1_PORT                       GPIOB
#define KEY2_PIN                        LL_GPIO_PIN_1
#define KEY2_PORT                       GPIOC
#define KEY3_PIN                        LL_GPIO_PIN_6
#define KEY3_PORT                       GPIOA
#define KEY4_PIN                        LL_GPIO_PIN_7
#define KEY4_PORT                       GPIOA
#define KEY5_PIN                        LL_GPIO_PIN_0  
#define KEY5_PORT                       GPIOC

typedef uint8 (* GET_IO_LEVEL_F)(void);

typedef struct _GLH_KEY_S
{
    GET_IO_LEVEL_F pfGetKeyIoLevel;
	uint8 u8ValidLevel;
	uint8 u8KeyCode;
	uint8 u8KeyCnt;
	uint8 u8KeyContinuousCnt;
	BOOL bKeyLongFlag;
    uint32 u32LongKeyDownTick;
}GLH_KEY_S;

static XDATA GLH_KEY_S s_sKey1 = {0}, s_sKey2 = {0}, s_sKey3 = {0}, s_sKey4 = {0}, s_sKey5 = {0};

static uint8 iGetKey1IoLevel(void)
{
    LL_GPIO_SetPinMode(KEY1_PORT, KEY1_PIN, LL_GPIO_MODE_INPUT);
    LL_GPIO_SetPinPull(KEY1_PORT, KEY1_PIN, LL_GPIO_PULL_UP);    
    if(LL_GPIO_IsInputPinSet(KEY1_PORT, KEY1_PIN) == 0)
    {
        LL_GPIO_SetPinMode(KEY1_PORT, KEY1_PIN, LL_GPIO_MODE_OUTPUT);
        LL_GPIO_SetPinOutputType(KEY1_PORT, KEY1_PIN, LL_GPIO_OUTPUT_PUSHPULL);
        LL_GPIO_SetOutputPin(KEY1_PORT, KEY1_PIN);
        return GLHKEY_VALID_LEVEL;
    }
    else
    {
        LL_GPIO_SetPinMode(KEY1_PORT, KEY1_PIN, LL_GPIO_MODE_OUTPUT);
        LL_GPIO_SetPinOutputType(KEY1_PORT, KEY1_PIN, LL_GPIO_OUTPUT_PUSHPULL);
        return !GLHKEY_VALID_LEVEL;
    }
}

static uint8 iGetKey2IoLevel(void)
{
    if(LL_GPIO_IsInputPinSet(KEY2_PORT, KEY2_PIN) == 0)
    {
        return GLHKEY_VALID_LEVEL;
    }
    else
    {
        return !GLHKEY_VALID_LEVEL;
    }
}

static uint8 iGetKey3IoLevel(void)
{
    if(LL_GPIO_IsInputPinSet(KEY3_PORT, KEY3_PIN) == 0)
    {
        return GLHKEY_VALID_LEVEL;
    }
    else
    {
        return !GLHKEY_VALID_LEVEL;
    }
}

static uint8 iGetKey4IoLevel(void)
{
    if(LL_GPIO_IsInputPinSet(KEY4_PORT, KEY4_PIN) == 0)
    {
        return GLHKEY_VALID_LEVEL;
    }
    else
    {
        return !GLHKEY_VALID_LEVEL;
    }
}

static uint8 iGetKey5IoLevel(void)
{
    if(LL_GPIO_IsInputPinSet(KEY5_PORT, KEY5_PIN) == 0)
    {
        return GLHKEY_VALID_LEVEL;
    }
    else
    {
        return !GLHKEY_VALID_LEVEL;
    }
}

void GLHKEY_Init(void)
{
    LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOA);
    LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOB);
    LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOC);
    
    s_sKey1.pfGetKeyIoLevel = iGetKey1IoLevel;
	s_sKey1.u8ValidLevel = GLHKEY_VALID_LEVEL;
	s_sKey1.u8KeyCode = 1;
	s_sKey1.u8KeyCnt = 0;
	s_sKey1.u8KeyContinuousCnt = 0;
    s_sKey1.bKeyLongFlag = FALSE;
    s_sKey1.u32LongKeyDownTick = GLHKEY_LONG_KEY_TICK;
    //KEY1按键因为需要复用输出，此处先设置为输出模式，输出高电平，扫描这个按键的时候再设置为输入模式。
    LL_GPIO_SetPinMode(KEY1_PORT, KEY1_PIN, LL_GPIO_MODE_OUTPUT);              
    LL_GPIO_SetPinOutputType(KEY1_PORT, KEY1_PIN, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetOutputPin(KEY1_PORT, KEY1_PIN);

	
    s_sKey2.pfGetKeyIoLevel = iGetKey2IoLevel;
	s_sKey2.u8ValidLevel = GLHKEY_VALID_LEVEL;
	s_sKey2.u8KeyCode = 2;
	s_sKey2.u8KeyCnt = 0;
	s_sKey2.u8KeyContinuousCnt = 0;
    s_sKey2.bKeyLongFlag = FALSE;
    s_sKey2.u32LongKeyDownTick = 50;
    LL_GPIO_SetPinMode(KEY2_PORT, KEY2_PIN, LL_GPIO_MODE_INPUT);
    LL_GPIO_SetPinPull(KEY2_PORT, KEY2_PIN, LL_GPIO_PULL_UP);
	
    s_sKey3.pfGetKeyIoLevel = iGetKey3IoLevel;
	s_sKey3.u8ValidLevel = GLHKEY_VALID_LEVEL;
	s_sKey3.u8KeyCode = 3;
	s_sKey3.u8KeyCnt = 0;
	s_sKey3.u8KeyContinuousCnt = 0;
    s_sKey3.bKeyLongFlag = FALSE;
    s_sKey3.u32LongKeyDownTick = 50;
    LL_GPIO_SetPinMode(KEY3_PORT, KEY3_PIN, LL_GPIO_MODE_INPUT);
    LL_GPIO_SetPinPull(KEY3_PORT, KEY3_PIN, LL_GPIO_PULL_UP);
    
    s_sKey4.pfGetKeyIoLevel = iGetKey4IoLevel;
	s_sKey4.u8ValidLevel = GLHKEY_VALID_LEVEL;
	s_sKey4.u8KeyCode = 4;
	s_sKey4.u8KeyCnt = 0;
	s_sKey4.u8KeyContinuousCnt = 0;
    s_sKey4.bKeyLongFlag = FALSE;
    s_sKey4.u32LongKeyDownTick = GLHKEY_LONG_KEY_TICK;
    LL_GPIO_SetPinMode(KEY4_PORT, KEY4_PIN, LL_GPIO_MODE_INPUT);
    LL_GPIO_SetPinPull(KEY4_PORT, KEY4_PIN, LL_GPIO_PULL_UP);
    
    s_sKey5.pfGetKeyIoLevel = iGetKey5IoLevel;
	s_sKey5.u8ValidLevel = GLHKEY_VALID_LEVEL;
	s_sKey5.u8KeyCode = 5;
	s_sKey5.u8KeyCnt = 0;
	s_sKey5.u8KeyContinuousCnt = 0;
    s_sKey5.bKeyLongFlag = FALSE;
    s_sKey5.u32LongKeyDownTick = GLHKEY_LONG_KEY_TICK;
    LL_GPIO_SetPinMode(KEY5_PORT, KEY5_PIN, LL_GPIO_MODE_INPUT);
    LL_GPIO_SetPinPull(KEY5_PORT, KEY5_PIN, LL_GPIO_PULL_UP);
}

void GLHKEY_DeInit(void)
{

}

//按键检测
static void iKeyCheck(GLH_KEY_S *sKey)
{
    if(sKey->pfGetKeyIoLevel == NULL)
    {
        return;
    }
    
	if(sKey->pfGetKeyIoLevel() == sKey->u8ValidLevel)
	{
		sKey->u8KeyCnt++;
		if(sKey->u8KeyCnt > 250)
		{
			sKey->u8KeyCnt = 250;   //防止计满溢出
		}
			
		if(sKey->u8KeyCnt == GLHKEY_SHORT_KEY_TICK)
		{
			GLHKEY_EventCb(sKey->u8KeyCode, E_KEY_ACTION_SHORT_KEYDOWN);
		}
		else if(sKey->u8KeyCnt == sKey->u32LongKeyDownTick)
		{
			GLHKEY_EventCb(sKey->u8KeyCode, E_KEY_ACTION_LONG_KEYDOWN);
			sKey->bKeyLongFlag = TRUE;
            sKey->u8KeyContinuousCnt = 0;				
		}
			
		if(sKey->bKeyLongFlag == TRUE)
		{
			sKey->u8KeyContinuousCnt++;
			if(sKey->u8KeyContinuousCnt == GLHKEY_CONTINUOUS_TICK)
			{
				sKey->u8KeyContinuousCnt = 0;
			    GLHKEY_EventCb(sKey->u8KeyCode, E_KEY_ACTION_CONTINUOUS);
			}
		}
	}
	else
	{
		if((sKey->u8KeyCnt >= GLHKEY_SHORT_KEY_TICK) && (sKey->u8KeyCnt < GLHKEY_LONG_KEY_TICK))
		{
			GLHKEY_EventCb(sKey->u8KeyCode, E_KEY_ACTION_SHORT_KEYUP);			
		}
		else if(sKey->u8KeyCnt >= GLHKEY_LONG_KEY_TICK)
		{
			GLHKEY_EventCb(sKey->u8KeyCode, E_KEY_ACTION_LONG_KEYUP);			
		}	
		sKey->u8KeyCnt = 0;
		sKey->bKeyLongFlag = FALSE;
        sKey->u8KeyContinuousCnt = 0;
	}
}

void GLHKEY_MainThread(void)
{
    static uint32 s_u32LastKeyCheckTime = 0;

	if(GulSystickCount - s_u32LastKeyCheckTime > GLHKEY_THREAD_INTERVAL)
	{
	    s_u32LastKeyCheckTime = GulSystickCount;
		
		iKeyCheck(&s_sKey1);
		iKeyCheck(&s_sKey2);
		iKeyCheck(&s_sKey3);
        iKeyCheck(&s_sKey4);
        iKeyCheck(&s_sKey5);
	}
}

#endif

#else
#include "main.h"
#include "glh_key.h"
#include "glh_sys_tick.h"

#ifndef JTUART_REC_BUFF_LEN
#define JTUART_REC_BUFF_LEN      50     //串口缓冲区大小
#endif

static uint16 s_u16Wp = 0;  
static uint16 s_u16Rp = 0;
uint8 s_au8RecBuff[JTUART_REC_BUFF_LEN] = {0};
uint8 s_au8KeyBuff[2] = {0};

static void iWriteByte(uint8 byte)
{
    s_au8RecBuff[s_u16Wp] = byte;
    s_u16Wp++;
	if(s_u16Wp == JTUART_REC_BUFF_LEN)
	{
		s_u16Wp = 0;
	}
}
/**
  * @brief  USART配置函数
  * @param  USARTx：USART模块，可以是USART1、USART2
  * @retval 无
  */
static void APP_ConfigUsart(USART_TypeDef *USARTx)
{
  /*使能时钟、初始化引脚、使能NVIC中断*/
  if (USARTx == USART1) 
  {
    /*使能GPIOB时钟*/
    LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOA);
    /*使能USART1时钟*/
    LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_USART1);
    
    /*GPIOB配置*/
    LL_GPIO_InitTypeDef GPIO_InitStruct;
    /*选择4号引脚*/
    GPIO_InitStruct.Pin = LL_GPIO_PIN_7;
    /*选择复用模式*/
    GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
    /*选择输出速度*/
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
    /*选择输出模式*/
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    /*选择上拉*/
    GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
    /*复用为USART1功能*/
    GPIO_InitStruct.Alternate = LL_GPIO_AF3_USART1;
    /*GPIOA初始化*/
    LL_GPIO_Init(GPIOA,&GPIO_InitStruct);
    
    /*设置USART1中断优先级*/
    NVIC_SetPriority(USART1_IRQn,0);
    /*使能USART1中断*/
    NVIC_EnableIRQ(USART1_IRQn);
  }
  
  /*配置USART功能*/
  LL_USART_InitTypeDef USART_InitStruct;
  /*设置波特率*/
  USART_InitStruct.BaudRate = 9600;
  /*设置数据长度*/
  USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
  /*停止位*/
  USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
  /*设置校验位*/
  USART_InitStruct.Parity = LL_USART_PARITY_NONE;
  USART_InitStruct.TransferDirection = LL_USART_DIRECTION_RX;
  USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
  USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
  /*USART初始化*/
  LL_USART_Init(USARTx, &USART_InitStruct);
  
  /*配置为全双工异步模式*/
  LL_USART_ConfigAsyncMode(USARTx);
  
  /*使能UART模块*/
  LL_USART_Enable(USARTx);
  
  /*使能接收数据寄存器非空中断*/
  LL_USART_EnableIT_RXNE(USARTx);
}

/**
  * @brief  USART中断处理函数
  * @param  USARTx：USART模块，可以是USART1、USART2
  * @retval 无
  */
void APP_UsartIRQCallback(USART_TypeDef *USARTx)
{
    iWriteByte(LL_USART_ReceiveData8(USARTx));
}

void USART1_IRQHandler(void)
{
    APP_UsartIRQCallback(USART1);
}

void GLHKEY_Init(void)
{
    APP_ConfigUsart(USART1);
}

void GLHKEY_MainThread(void)
{
    static  uint8 s_u8LastKeyDownCode = 0;
        
    while (s_u16Rp != s_u16Wp)
    {
        s_au8KeyBuff[0] = s_au8KeyBuff[1];
        s_au8KeyBuff[1] = s_au8RecBuff[s_u16Rp];
        
        if(s_au8KeyBuff[0] + s_au8KeyBuff[1] == 0xff)
        {
            switch(s_au8KeyBuff[0])
            {
                case 0x01:
                {
                    s_u8LastKeyDownCode = KEY_CODE_ON_OFF;
                    GLHKEY_EventCb(KEY_CODE_ON_OFF, E_KEY_ACTION_SHORT_KEYDOWN);
                }
                break;
                
                case 0x02:
                {
                    s_u8LastKeyDownCode = KEY_CODE_V_C;
                    GLHKEY_EventCb(KEY_CODE_V_C, E_KEY_ACTION_SHORT_KEYDOWN);
                }
                break;
                
                case 0x10:
                {
                    s_u8LastKeyDownCode = KEY_CODE_DEC;
                    GLHKEY_EventCb(KEY_CODE_DEC, E_KEY_ACTION_SHORT_KEYDOWN);
                }
                break;
                
                case 0x04:
                {
                    s_u8LastKeyDownCode = KEY_CODE_INC;
                    GLHKEY_EventCb(KEY_CODE_INC, E_KEY_ACTION_SHORT_KEYDOWN);
                }
                break;
                
                case 0x08:
                {
                    s_u8LastKeyDownCode = KEY_CODE_HOT;
                    GLHKEY_EventCb(KEY_CODE_HOT, E_KEY_ACTION_SHORT_KEYDOWN);
                }
                break;
                
                case 0x20:
                {
                    s_u8LastKeyDownCode = KEY_CODE_RELEASE;
                    GLHKEY_EventCb(KEY_CODE_RELEASE, E_KEY_ACTION_SHORT_KEYDOWN);
                }
                break;
                
                case 0x00:
                {
                    GLHKEY_EventCb(s_u8LastKeyDownCode, E_KEY_ACTION_SHORT_KEYUP);
                }
                break;
                
                default:
                    break;
            }
        }
        
        s_u16Rp++;
		if(s_u16Rp == JTUART_REC_BUFF_LEN )
		{
		    s_u16Rp = 0;
		}
    }
}
#endif
