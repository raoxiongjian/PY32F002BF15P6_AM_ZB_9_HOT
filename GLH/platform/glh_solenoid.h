/*
* Copyright (c) 2023, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_sys_tick.h
* 文件标识：
* 摘 要：
*   系统时钟模块，实现开机到当前的时间
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2023年3月31日
*/
#ifndef __GLH_SOLENOID_H__
#define __GLH_SOLENOID_H__
#include "glh_typedef.h"

typedef enum _SOLENOID_STATUS_E
{
    E_SOLENOID_STATUS_OPEN,
    E_SOLENOID_STATUS_CLOSE,
}SOLENOID_STATUS_E;

/**
    @功能: 初始化电磁阀
    @参数:
    @返回:
*/
void GLHSOLENOID_Init(void);

/**
    @功能: 电磁阀状态设置
    @参数:
    @返回:
*/
void GLHSOLENOID_Set(SOLENOID_STATUS_E eSolenoidStaus);

#endif //__GLH_LED_H__


