/*
* Copyright (c) 2020, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_key.h
* 文件标识：
* 摘 要： 
*   按键处理，采用查询方式实现
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年8月9日
*/
#ifndef __GLH_KEY_H__
#define __GLH_KEY_H__
#include "glh_typedef.h"

#ifdef SUPPORT_KEY

#define KEY_CODE_ON_OFF         1
#define KEY_CODE_V_C            2
#define KEY_CODE_HOT            3
#define KEY_CODE_INC            4
#define KEY_CODE_DEC            5
#define KEY_CODE_RELEASE        6

//按键动作
typedef enum _GLHKEY_ACTION_E
{
    E_KEY_ACTION_UNKOWN = 0, 
    E_KEY_ACTION_SHORT_KEYDOWN,   //短按键按下
    E_KEY_ACTION_SHORT_KEYUP,     //短按键抬起
    E_KEY_ACTION_LONG_KEYDOWN,    //长按键按下
    E_KEY_ACTION_LONG_KEYUP,      //长按键抬起
    E_KEY_ACTION_DOUBLE_KEY,      //双击
	E_KEY_ACTION_CONTINUOUS,      //按键连续触发
}GLHKEY_ACTION_E;

/**
    @功能: 按键事件回调
    @参数: 
            u8Code[in]: 按键键值
            u8Action[in]: 按键动作，参考GLHKEY_ACTION_E
    @返回: 
*/
extern void GLHKEY_EventCb(uint8 u8Code, uint8 u8Action);

/**
    @功能: 初始化按键模块
    @参数:
    @返回:
*/
void GLHKEY_Init(void);


/**
    @功能: 恢复按键模块到默认状态
    @参数:
    @返回:
*/
void GLHKEY_DeInit(void);

/**
    @功能: 按键主线程
    @参数:
    @返回:
*/
void GLHKEY_MainThread(void);

#endif //SUPPORT_KEY

#endif //__JT_KEY_H__
