/*
* Copyright (c) 2023, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_adc.c
* 文件标识：
* 摘 要：
*    adc模块
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2023年7月5日
*/

#include "glh_adc.h"
#include "main.h"

/**
  * @brief  ADC配置函数.
  * @param  无
  * @retval 无
  */
static void APP_AdcConfig(void)
{
    /* 设置ADC时钟 */
    LL_ADC_SetClock(ADC1, LL_ADC_CLOCK_SYNC_PCLK_DIV8);

    /* 设置12位分辨率 */
    LL_ADC_SetResolution(ADC1, LL_ADC_RESOLUTION_12B);

    /* 设置数据右对齐 */
    LL_ADC_SetDataAlignment(ADC1, LL_ADC_DATA_ALIGN_RIGHT);

    /* 设置低功耗模式无 */
    LL_ADC_SetLowPowerMode(ADC1, LL_ADC_LP_MODE_NONE);

    /* 设置通道转换时间 */
    LL_ADC_SetSamplingTimeCommonChannels(ADC1, LL_ADC_SAMPLINGTIME_13CYCLES_5);

    /* 设置触发源为SOFTWARE */
    LL_ADC_REG_SetTriggerSource(ADC1, LL_ADC_REG_TRIG_SOFTWARE);

//    /* 设置上升沿触发转换 */
//    LL_ADC_REG_SetTriggerEdge(ADC1, LL_ADC_REG_TRIG_EXT_RISING);

    /* 设置转换模式为单次转换 */
    LL_ADC_REG_SetContinuousMode(ADC1, LL_ADC_REG_CONV_SINGLE);

    /* 设置过载管理模式为覆盖上一个值 */
    LL_ADC_REG_SetOverrun(ADC1, LL_ADC_REG_OVR_DATA_OVERWRITTEN);

    /* 设置非连续模式为不使能 */
    LL_ADC_REG_SetSequencerDiscont(ADC1, LL_ADC_REG_SEQ_DISCONT_DISABLE);

    /* 设置通道0为转换通道 */
    LL_ADC_REG_SetSequencerChannels(ADC1, LL_ADC_CHANNEL_0);
  
    /* 配置内部转换通道无 */
    LL_ADC_SetCommonPathInternalCh(__LL_ADC_COMMON_INSTANCE(ADC1), LL_ADC_PATH_INTERNAL_VREFINT);
}

/**
  * @brief  ADC校准函数.
  * @param  无
  * @retval 无
  */
static void APP_AdcCalibrate(void)
{
    __IO uint32_t wait_loop_index = 0;

    if (LL_ADC_IsEnabled(ADC1) == 0)
    {
        /* 使能校准 */
        LL_ADC_StartCalibration(ADC1);

        while (LL_ADC_IsCalibrationOnGoing(ADC1) != 0)
        {

        }

        /* ADC校准结束和使能ADC之间的延时最低4个ADC Clock */
        LL_mDelay(1);
    }
}

/**
  * @brief  ADC使能函数.
  * @param  无
  * @retval 无
  */
static void APP_AdcEnable(void)
{
    /* 使能ADC */
    LL_ADC_Enable(ADC1);

    /* 使能ADC 稳定时间，最低8个ADC Clock */
    LL_mDelay(1);
}

void GLHADC_Init(uint16 u16AdcCH)
{
    __IO uint32_t wait_loop_index = 0;
    
    if(u16AdcCH & GLHADC_CHANNEL_0)
    {
        /* 使能GPIOB时钟 */
        LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOB);
        /* 配置PB1为模拟模式 */
        LL_GPIO_SetPinMode(GPIOB, LL_GPIO_PIN_1, LL_GPIO_MODE_ANALOG);    
    }
    
    if(u16AdcCH & GLHADC_CHANNEL_2)
    {
        /* 使能GPIOA时钟 */
        LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOA);
        /* 配置PA4为模拟模式 */
        LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_4, LL_GPIO_MODE_ANALOG);    
    }
    
    /* 使能ADC1时钟 */
    LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_ADC1);

    /* 配置ADC参数 */
    APP_AdcConfig();
  
    /* ADC校准 */
    APP_AdcCalibrate();

    /* ADC使能 */
    APP_AdcEnable();

    /* 选择1.5V为参考电压 */
    LL_ADC_SetVrefBufferVoltage(ADC1,LL_ADC_VREFBUF_1P5V);
  
    /* 使能参考电压 */
    LL_ADC_EnableVrefBufferVoltage(ADC1); 

    /* Vrefint 等待稳定时间 */
    wait_loop_index = ((LL_ADC_DELAY_VREFINT_STAB_US * (SystemCoreClock / (100000 * 2))) / 10);
    while(wait_loop_index != 0)
    {
        wait_loop_index--;
    }    
}

uint16 GLHADC_Get(uint16 u16AdcCH)
{
    uint16 u16AdValue = 0;
    
    //必须失能ADC，后面设置的ADC通道才能生效
    LL_ADC_Disable(ADC1);    
    if(u16AdcCH == GLHADC_CHANNEL_0)
    {
        /* 设置通道0为转换通道 */
        LL_ADC_REG_SetSequencerChannels(ADC1, LL_ADC_CHANNEL_0);    
    }
    else if(u16AdcCH == GLHADC_CHANNEL_2)
    {
        /* 设置通道2为转换通道 */
        LL_ADC_REG_SetSequencerChannels(ADC1, LL_ADC_CHANNEL_2);    
    }
    else
    {
        return 0;
    }
    LL_ADC_Enable(ADC1);
    
    /* 开始ADC转换(如果是软件触发则直接开始转换) */
    LL_ADC_REG_StartConversion(ADC1);
    
    //等待转换完成
    while(LL_ADC_IsActiveFlag_EOC(ADC1)==0);
    LL_ADC_ClearFlag_EOC(ADC1);
    
    //读取AD值
    u16AdValue = LL_ADC_REG_ReadConversionData12(ADC1);
    
    return u16AdValue;
}
