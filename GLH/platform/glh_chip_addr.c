/*
* Copyright (c) 2022, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_chip_addr.c
* 文件标识：
* 摘 要：板子上会使用多个相同mcu，获取此mcu为板子上的哪一个位置上的。
*   
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2022.11.11
*/
#include "glh_chip_addr.h"
#include "main.h"

static GPIO_InitTypeDef   GPIO_InitStruct;

void GLH_CHIP_ADDR_Init(void)
{
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOF_CLK_ENABLE();
    
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;     
    GPIO_InitStruct.Pin = GPIO_PIN_7;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;     
    GPIO_InitStruct.Pin = GPIO_PIN_5;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

uint8 GLH_CHIP_ADDR_Get(void)
{
    uint8 u8Addr = 0;
    
    if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_5) == GPIO_PIN_SET)
    {
        u8Addr |= 0x01;
    }
    
    if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_7) == GPIO_PIN_SET)
    {
        u8Addr |= 0x02;
    }
    
    return u8Addr;
}

