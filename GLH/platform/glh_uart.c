/*
* Copyright (c) 2022, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_uart.c
* 文件标识：
* 摘 要：
*   
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2022.11.11
*/
#include "main.h"
#include "glh_uart.h"

#ifndef UART_REC_BUFF_LEN
#define GLHUART_REC_BUFF_LEN      50     //串口缓冲区大小
#endif
static UART_HandleTypeDef UartHandle;
static uint16 XDATA s_u8Wp = 0;  
static uint16 XDATA s_u8Rp = 0;
static uint8 XDATA s_au8RecBuff[GLHUART_REC_BUFF_LEN] = {0}; 

static void iWriteByte(uint8 byte)
{
    s_au8RecBuff[s_u8Wp] = byte;
    s_u8Wp++;
	if(s_u8Wp == GLHUART_REC_BUFF_LEN)
	{
		s_u8Wp = 0;
	}
}

void GLHUART_Init(void)
{
    GPIO_InitTypeDef  GPIO_InitStruct;
  //====================
  //USART2初始化
  //====================
    __HAL_RCC_USART2_CLK_ENABLE();
    __HAL_RCC_GPIOF_CLK_ENABLE();
    UartHandle.Instance          = USART2;
    UartHandle.Init.BaudRate     = 250000;
    UartHandle.Init.WordLength   = UART_WORDLENGTH_8B;
    UartHandle.Init.StopBits     = UART_STOPBITS_1;
    UartHandle.Init.Parity       = UART_PARITY_NONE;
    UartHandle.Init.HwFlowCtl    = UART_HWCONTROL_NONE;
    UartHandle.Init.Mode         = UART_MODE_RX;

    if (HAL_UART_Init(&UartHandle) != HAL_OK)
    {
        Error_Handler();
    }
    /**USART2 GPIO Configuration
    PA0     ------> USART2_TX
    PA1     ------> USART2_RX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_2;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF4_USART2;
    HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);
    /* 使能NVIC */
    HAL_NVIC_SetPriority(USART2_IRQn, 0, 1);
    HAL_NVIC_EnableIRQ(USART2_IRQn);
    __HAL_UART_ENABLE_IT(&UartHandle, UART_IT_RXNE);
}

void GLHUART_SendByte(uint8 u8Byte)
{

}

void GLHUART_SendBuff(const uint8 *pu8Buff, uint8 u8Len)
{
    uint8 i = 0;
    if (pu8Buff != NULL && u8Len > 0)
    {
        for (i=0; i<u8Len; i++)
        {
            GLHUART_SendByte(pu8Buff[i]);
        }
    }
}

void GLHUART_MainLoop(void)
{
    while (s_u8Rp != s_u8Wp)
    {
        GLHUART_RecCallback(s_au8RecBuff[s_u8Rp]);
		
        s_u8Rp++;
		if(s_u8Rp == GLHUART_REC_BUFF_LEN )
		{
		    s_u8Rp = 0;
		}
    }
}

void USART2_IRQHandler(void)
{
    if(__HAL_UART_GET_FLAG(&UartHandle, UART_FLAG_RXNE))
    {
        iWriteByte((((&UartHandle)->Instance)->DR));
    }
}
