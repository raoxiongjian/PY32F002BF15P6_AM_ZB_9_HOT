/*
* Copyright (c) 2023, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_solenoid.c
* 文件标识：
* 摘 要：
*   电磁阀处理
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2023年7月5日
*/
#include "main.h"
#include "glh_solenoid.h"

#define SOLENOID_PIN        LL_GPIO_PIN_3
#define SOLENOID_PORT       GPIOB

void GLHSOLENOID_Init(void)
{
    LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOB);
    
    LL_GPIO_SetPinMode(SOLENOID_PORT, SOLENOID_PIN, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(SOLENOID_PORT, SOLENOID_PIN, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_ResetOutputPin(SOLENOID_PORT, SOLENOID_PIN);
}

void GLHSOLENOID_Set(SOLENOID_STATUS_E eSolenoidStaus)
{
    if(eSolenoidStaus == E_SOLENOID_STATUS_OPEN)
    {
        LL_GPIO_SetOutputPin(SOLENOID_PORT, SOLENOID_PIN);
    }
    else if(eSolenoidStaus == E_SOLENOID_STATUS_CLOSE)
    {
        LL_GPIO_ResetOutputPin(SOLENOID_PORT, SOLENOID_PIN);
    }
}

