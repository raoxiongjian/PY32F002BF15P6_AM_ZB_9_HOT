/*
* Copyright (c) 2023, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_sys_tick.h
* 文件标识：
* 摘 要：
*   系统时钟模块，实现开机到当前的时间
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2023年3月31日
*/
#ifndef __GLH_LED_H__
#define __GLH_LED_H__
#include "glh_typedef.h"
#define LED1_FLAG                   (1ul << 0)    //LED11
#define LED2_FLAG                   (1ul << 1)    //LED21 LED22
#define LED3_FLAG                   (1ul << 2)    //LED31 LED32
#define LED4_FLAG                   (1ul << 3)    //LED61 LED62
#define LED5_FLAG                   (1ul << 4)    //LED51 LED52
#define LED6_FLAG                   (1ul << 5)    //LED41 LED42
#define LED7_FLAG                   (1ul << 6)    //LED_VC
#define LED8_FLAG                   (1ul << 7)    //LED_HOT
#define LED9_FLAG                   (1ul << 8)    //LED_G
#define LED1_OUTSIDE_FLAG           (1ul << 9)
#define LED_ALL                      0xffff

void GLHLED_Init(void);

void GLHLED_TurnOn(uint16 u16Led);

void GLHLED_TurnOff(uint16 u16Led);

#endif //__GLH_LED_H__


