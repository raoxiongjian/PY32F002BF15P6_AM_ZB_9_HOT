/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_key.c
* 文件标识：
* 摘 要：
*   按键处理
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年8月9日
*/
#include "include/ca51f005_config.h"		
#include "include/ca51f005sfr.h"
#include "include/ca51f005xsfr.h"
#include "include/gpiodef_f005.h"
#include "include/system_clock.h"
#include "include/uart.h"
#include "include/delay.h"
#include <intrins.h>
#include "include/flash.h"
#include "glh_tm1651.h"
#include "glh_delay.h"

static void iI2cStart(void)
{
//	SCK_1;
//	SDA_1;
//	delay_us(2);
//	SDA_0;
//	delay_us(2);
//	SCK_0;
}

static void iI2cWaitAck(void)
{
//	uint8 cnt=0;
//	SCK_0;
//	delay_us(2);
//	SDA_1;
//	delay_us(2);
//	SCK_1;
//		
//	SDA_IN;
//	while(SDA)
//	{
//		cnt++;
//		if(cnt>60)
//        {
//			SDA_1;
//			break;
//		}
//	}
//	SDA_OUT;
}

static void iI2cAck(void)
{
//    SCK_0;
//    delay_us(2);
//    SDA_0;
//    delay_us(2);
//    SCK_1;
//    delay_us(2);
//    SCK_0;
//    delay_us(2);
}


static void iI2cStop(void)
{
//	SCK_0;
//	delay_us(2);
//	SDA_0;
//	delay_us(2);
//	SCK_1;
//	delay_us(2);
//	SDA_1;
}


static void iI2cWriteByte(unsigned char dat)
{
//	unsigned char i;
//	for(i=0;i<8;i++)
//	{
//		SCK_0;
//		delay_us(2);
//		if(dat&0x01)
//        {
//            SDA_1;
//        }
//        else
//        {
//            SDA_0;
//        }
//        delay_us(2);
//		dat>>=1;
//		SCK_1;
//		delay_us(2);
//	}
}

static uint8 iI2cReadByte(void)
{
//    uint8 i = 0;
//    uint8 u8Temp = 0;

//    SDA_IN;
//    delay_us(2);
//    for(i = 0; i < 8; i++)
//    {
//        SCK_1;
//        delay_us(2);
//        SCK_0;
//        delay_us(2);
//        SCK_1;
//        delay_us(2);
//        if(SDA == 1)
//        {
//            u8Temp |= (1 << i);
//        }
//        delay_us(2);
//    }
//    SCK_0;
//    delay_us(2);
//    
//    return u8Temp;
}

void GLHTM1651_Init(void)
{
//    P00F = OUTPUT;
//    P01F = OUTPUT;
}

void GLHTM1651_SmgDisplay(uint8 *dat,uint8 length,uint8 led_brightness)
{
//	uint8 cnt=0;
//		
//	iI2cStart();
//	iI2cWriteByte(0x40); 
//	iI2cWaitAck();
//	iI2cStop();
//		
//	iI2cStart();
//	iI2cWriteByte(0xc0);
//	iI2cWaitAck();
//	for(cnt=0;cnt<length;cnt++)
//	{
//		iI2cWriteByte(*dat++);
//		iI2cWaitAck();
//	}
//	iI2cStop();
//        
//	iI2cStart();
//	iI2cWriteByte(led_brightness);
//	iI2cWaitAck();
//	iI2cStop();
}

void GLHTM1651_ReadKey(uint8 *pu8Buff)
{
//    iI2cStart();
//    iI2cWriteByte(0x42);
//    iI2cWaitAck();    
//    *pu8Buff = iI2cReadByte();
//    iI2cWaitAck(); 
//    iI2cStop();
}

