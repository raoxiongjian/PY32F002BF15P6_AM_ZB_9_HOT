/*
* Copyright (c) 2016, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_rf_433.c
* 文件标识：
* 摘 要：
*  433M数据接收
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2021年6月25日
*/
#include "glh_rf_433.h"
#include "glh_sys_tick.h"
#include "glh_watchdog.h"
#include "glh_delay.h"
#include "gpio.h"
#include "sysctrl.h"
#include "bt.h"
#include "adt.h"

#include "glh_sys_tick.h"
#include "glh_typedef.h"
#include "glh_app_process.h"
#include "glh_watchdog.h"
#include "gpio.h"
#include "sysctrl.h"
#include "glh_pwm.h"
#include "glh_delay.h"

//引导码     4.54ms高电平
// 1         1.66ms高电平
// 0         0.48ms高电平
#ifdef SUPPORT_RF433

uint32 g_u32RfData = 0;  //数据

static BOOL s_bRfStart = FALSE; //是否接收到了引导码
static BOOL  s_bRfRecEnd = FALSE;   //是否接收完成
static uint8  s_u8RfBitBum = 0; //接收到的数据位数
static uint8 s_u8HiLeveCnt = 0;

//函数申明
#define iRfReadGpio()      hal_gpio_read(JTRF433_PIN)      // 接收rf数据的接口

void Tim_IRQHandler(uint8 param)       //100us中断一次
{
    if(param == 0)
    {
        M0P_TIM0->ICLR_f.UIF = FALSE;   //清除中断标记
        
        if(s_bRfRecEnd)
        {
            return;
        }

        if(Gpio_GetInputIO(JTRF433_PORT, JTRF433_PIN))
        {
            s_u8HiLeveCnt++;
            if(s_u8HiLeveCnt > 250)
            {
                s_u8HiLeveCnt = 250;
            }
            
        }
        else
        { 
            if(s_u8HiLeveCnt == 0)
            {
                return;
            }
            
            if(s_u8HiLeveCnt > 47)     //无效码
            {
                s_bRfStart = FALSE;
            }
            else if(s_u8HiLeveCnt>43 && s_u8HiLeveCnt < 47)    //引导码
            {
                s_bRfStart = TRUE;
                s_u8RfBitBum = 0;
                g_u32RfData = 0;
                
                s_u8HiLeveCnt = 0;
                return;
            }
            
            if(s_bRfStart)
            {
                if(s_u8HiLeveCnt>10 && s_u8HiLeveCnt<20)        //1
                {
                    g_u32RfData |= (1 << s_u8RfBitBum);
                    s_u8RfBitBum++;
                }
                else if(s_u8HiLeveCnt>2 && s_u8HiLeveCnt<6)          //0
                {
                    s_u8RfBitBum++;
                }
                else
                {
                    s_bRfStart = FALSE;
                }
            }
            
            if(s_u8RfBitBum >= 32)
            {
                s_bRfStart = FALSE;
                s_bRfRecEnd = TRUE;
                s_u8RfBitBum = 0;
            }
            
            s_u8HiLeveCnt = 0;
        }
    }
}
void iTimer0Init(void)
{
	stc_bt_cfg_t tim0;
	stc_clk_config_t stcClkCfg;
	stcClkCfg.enClkSrc  = ClkRCH;
	stcClkCfg.enHClkDiv = ClkDiv1;
	stcClkCfg.enPClkDiv = ClkDiv1;

	Clk_Init(&stcClkCfg);
	
	//打开GPIO、BT外设时钟
	Clk_SetPeripheralGate(ClkPeripheralBt, TRUE);
	tim0.enGate = BtGateDisable;
	tim0.enPRS = BtPCLKDiv4;
	tim0.enCT = BtTimer;
	tim0.enMD = BtMode2;
	//tim0.pfnTim0Cb = Timer0_IRQHandler;
	Bt_ARRSet(TIM0,65536-603);
    Bt_ClearIntFlag(TIM0);
	Bt_Init(TIM0,&tim0);
	Bt_EnableIrq(TIM0);
    EnableNvic(TIM0_IRQn, 0, TRUE);
	Bt_Run(TIM0);
}

void GLHRF433_Init(void)
{
    stc_gpio_cfg_t stcGpioCfg;
    
    ///< 打开GPIO外设时钟门控
    Sysctrl_SetPeripheralGate(SysctrlPeripheralGpio, TRUE);
    
    ///< 端口方向配置->输入
    stcGpioCfg.enDir = GpioDirIn;
    ///< 端口驱动能力配置->高驱动能力
    stcGpioCfg.enDrv = GpioDrvL;
    ///< 端口上下拉配置->无
    stcGpioCfg.enPu = GpioPuDisable;
    stcGpioCfg.enPd = GpioPdDisable;
    ///< 端口开漏输出配置->开漏输出关闭
    stcGpioCfg.enOD = GpioOdDisable;
    ///< GPIO IO USER KEY初始化
    Gpio_Init(JTRF433_PORT, JTRF433_PIN, &stcGpioCfg); 
    
    iTimer0Init();
  
    
    s_bRfStart = FALSE;
    s_bRfRecEnd = FALSE;
    s_u8RfBitBum = 0;
    g_u32RfData = 0;

}

void GLHRF433_MainThread(void)
{
    if (s_bRfRecEnd)
    {
        s_bRfRecEnd = FALSE;

        GLHRF433_EventCb();
        
        g_u32RfData = 0;
    }
}
#if 0
static void iCheckRf433DataThread(void)
{
    uint8 io = iRfReadGpio();
    uint8 tick = 0;

    //超时处理
    if (s_bRfStart && time_after_eq(g_uxSysTick, s_u32TimeoutTick))
    {
        s_bRfStart = FALSE;
    }

    // IO没有变化，或者当前为高电平
    if (io != 0 || s_u8LastLevel == io)
    {
        s_u8LastLevel = io;
        return;
    }

    s_u8LastLevel = io;

    //获取低电平电平时间
    while (iRfReadGpio() == 0)
    {
        JTDL_DelayUs(RF_UNIT_DELAY);
        tick++;
        if (tick >= RF_MAX_TICK)
        {
            s_bRfStart = FALSE;
            return;
        }
    }

    s_u8LastLevel = 1;

    //起始信号12.2ms
//    if (tick > (11500/RF_UNIT_DELAY))
    if (tick > (8000/RF_UNIT_DELAY))
    {
        s_bRfStart = TRUE;
        s_u8RfBitBum = 0;
        g_u32RfData = 0;
        s_u32TimeoutTick = g_uxSysTick + RF_REC_TIMEOUT_TICK;
        return;
    }

    if (s_bRfStart)
    {
        if(tick <= (800/RF_UNIT_DELAY) && tick >= (200/RF_UNIT_DELAY)) // 1, 0.6ms
        {
            g_u32RfData <<= 1;  //因为先发地址高位，所以要向右移动
            g_u32RfData |= 0x00000001;
        }
        else if(tick <= (2000/RF_UNIT_DELAY) && tick >= (1100/RF_UNIT_DELAY)) // 0, 1.3ms
        {
            g_u32RfData <<= 1;  //因为先发地址高位，所以要向右移动
        }
        else
        {
            s_bRfStart = FALSE;
            return;
        }

        s_u8RfBitBum++; //收到一位后RF_Num加1
        if(s_u8RfBitBum > 23) //如果RF_Num>23，即等于24（为保证可靠，用大于）时，说明数据已经收完
        {
            s_bRfRecEnd = TRUE; //数据收完后接收结束标志置位
            s_bRfStart = FALSE; //启动标志清零
        }
    }
}
#endif
#endif
