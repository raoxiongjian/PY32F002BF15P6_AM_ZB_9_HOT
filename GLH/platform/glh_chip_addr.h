/*
* Copyright (c) 2022, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_pwm.h
* 文件标识：
* 摘 要： 
*     通过定时器产生pwm
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2021年7月5日
*/
#ifndef __GLH_CHIP_ADDR_H__
#define __GLH_CHIP_ADDR_H__
#include "glh_typedef.h"

#ifdef SUPPORT_CHIP_ADDR

void GLH_CHIP_ADDR_Init(void);

uint8 GLH_CHIP_ADDR_Get(void);
#endif //ifdef SUPPORT_CHIP_ADDR

#endif //__GLH_CHIP_ADDR_H__

