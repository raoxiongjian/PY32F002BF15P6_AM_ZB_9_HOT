/*
* Copyright (c) 2022, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_flash.h
* 文件标识：
* 摘 要：
*    读写flash模块
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2022年6月22日
*/
#ifndef __GLH_FLASH_H__
#define __GLH_FLASH_H__
#include "glh_typedef.h"

#define GLHFLASH_START_ADDR        0
#define GLHFLASH_END_ADDR          0x5ff
#define GLHFLASH_PAGE_SIZE         512        //flash一页的大小
#define GLHFLASH_MAX_SIZE          (512*128)   // 32k

/*
    @功能: 初始化flash模块
    @参数: 
    @返回: 
*/
void GLHFLASH_Init(void);

/*
    @功能: 擦除指定区域的flash
    @参数: u32Addr[in]:   擦除起始地址
                u16Len[in]:    擦除长度
    @返回: 
    @说明：  CA51F005芯片是以4字节为单位进行存储，所以u16Len必须为4的整数倍
*/
void GLHFLASH_EraseBlock(uint32 u32Addr, uint16 u16Len);

/*
    @功能: 从flash中读一个字节
    @参数: u32Addr[in]: 需要读取的地址
    @返回: 读取的flash中的数据
*/
//uint8 GLHFLASH_ReadByte(uint32 u32Addr);

/*
    @功能: 写一字节的数据到指定的flash地址
    @参数: u16Addr[in]:   写入的地址
               u8Byte[in]:   写入的数据
    @返回: 无
*/
//void GLHFLASH_WriteByte(uint16 u16Addr, uint8 u8Byte);

/*
    @功能: 从指定的flash地址读取多个字节的数据
    @参数: u32Addr[in]:   需要写入数据的flash起始地址
               pu8Buff[in]: 数据缓存区，不能为空
               u16Len[in]: 需要写入的数据长度，不能为0，也不能大于可用的flash长度
    @返回: 原则上来说是需要返回值的
    @说明：  CA51F005芯片是以4字节为单位进行存储，所以u16Len必须为4的整数倍
*/
void GLHFLASH_ReadBytes(uint32 u32Addr, uint8 * pu8Buff, uint16 u16Len);

/*
    @功能: 写入多个字节的数据到指定的flash区域
    @参数: u32Addr[in]:   需要写入数据的flash起始地址
               pu8Buff[in]: 数据缓存区，不能为空
               u16Len[in]: 需要写入的数据长度，不能为0，也不能大于可用的flash长度
    @返回: 原则上来说，是需要返回值的
    @说明：  CA51F005芯片是以4字节为单位进行存储，所以u16Len必须为4的整数倍
*/
void GLHFLASH_WriteBytes(uint32 u32Addr, uint8 *pu8Buff, uint16 u16Len);

/**
    @功能: 从FLASH中读取设备的ID
    @参数: pu8Buff[in]: 存储设备ID的缓存，长度为16个字节
    @返回: 
*/
void GLHFLASH_ReadDevId(uint8 *pu8Buff);

void GLHFLASH_OpitonBytesConfig(void);

#endif //__GLH_FLASH_H__

