/*
* Copyright (c) 2020, 深圳市君同科技有限公司
* All rights reserved.
*
* 文件名称：jt_gpio.c
* 文件标识：
* 摘 要：
*   gpio操作通用接口，只支持设置GPIO为普通GPIO状态，包括输入、输出、中断，不支持复用（PWM、AD、UART等）
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2020年12月15日
*/
#include "jt_gpio.h"

#define INVALID_GPIO_NUM    0xFF  //无效的GPIO编号

typedef struct _IRQ_INFO_S
{
    uint8 m_u8GpioNum;
    JTGPIO_IrqCallback_F m_pfnIrqCallback;
}IRQ_INFO_S;

static IRQ_INFO_S s_sIrqInfo[JTGPIO_MAX_GPIO_NUM];

static void iIoHanderCallback(uint32 u32Val);

void JTGPIO_DirOutput(uint8 u8GpioNum, uint8 u8OutputType)
{
    // 引脚复用成普通IO
    if (u8GpioNum == GPIO_PIN_11 || u8GpioNum == GPIO_PIN_12 || u8GpioNum == GPIO_PIN_13)
    {
        gpio_mux_ctl(u8GpioNum, 1);
    }
    else
    {
        gpio_mux_ctl(u8GpioNum, 0);
    }
    
    gpio_fun_sel(u8GpioNum, GPIO_Dx);
    gpio_direction_output(u8GpioNum);
}

void JTGPIO_DirInput(uint8 u8GpioNum, uint8 u8InputType)
{
    // 引脚复用成普通IO
    if (u8GpioNum == GPIO_PIN_11 || u8GpioNum == GPIO_PIN_12 || u8GpioNum == GPIO_PIN_13)
    {
        gpio_mux_ctl(u8GpioNum, 1);
    }
    else
    {
        gpio_mux_ctl(u8GpioNum, 0);
    }
    
    gpio_fun_sel(u8GpioNum, GPIO_Dx);
    gpio_direction_input(u8GpioNum, u8InputType);
}

void JTGPIO_SetValue(uint8 u8GpioNum, uint8 u8Value)
{
    if (u8Value != 0)
    {
         gpio_output_high(u8GpioNum);
    }
    else
    {
        gpio_output_low(u8GpioNum);
    }
}

uint8 JTGPIO_GetValue(uint8 u8GpioNum)
{
    return gpio_input_val(u8GpioNum);
}

void JTGPIO_EnableIrq(uint8 u8GpioNum, uint8 u8IrqType)
{
    gpio_fun_inter(u8GpioNum, u8IrqType);
}

void JTGPIO_DisableIrq(uint8 u8GpioNum)
{
    gpio_fun_inter(u8GpioNum, NOT_INT);
}

void JTGPIO_RegisterIrqCallback(uint8 u8GpioNum, JTGPIO_IrqCallback_F pfnCallback)
{
    static BOOL s_bIrqInit = FALSE;
    uint8 i = 0, index = 0;

    JTASSERT(pfnCallback != NULL);

    if (!s_bIrqInit)
    {
        s_bIrqInit = TRUE;
        
        gpio_Register_Callback(iIoHanderCallback);

        for (i=0; i<JTGPIO_MAX_GPIO_NUM; i++)
        {
            s_sIrqInfo[i].m_u8GpioNum = INVALID_GPIO_NUM;
            s_sIrqInfo[i].m_pfnIrqCallback = NULL;
        }
    }

    index = JTGPIO_MAX_GPIO_NUM;

    for (i=0; i<JTGPIO_MAX_GPIO_NUM; i++)
    {   
        //查询是否有重复注册
        if (s_sIrqInfo[i].m_u8GpioNum == u8GpioNum)
        {
            return;
        }

        //得到可用的中断信息存储index
        if (s_sIrqInfo[i].m_u8GpioNum == INVALID_GPIO_NUM)
        {
            index = i;
        }
    }

    if (index != JTGPIO_MAX_GPIO_NUM)
    {
        s_sIrqInfo[index].m_u8GpioNum = u8GpioNum;
        s_sIrqInfo[index].m_pfnIrqCallback = pfnCallback;
    }
}

static void iIoHanderCallback(uint32 u32Val)
{
    uint8 i = 0;

    for (i=0; i<JTGPIO_MAX_GPIO_NUM; i++)
    {
        if (s_sIrqInfo[i].m_u8GpioNum != INVALID_GPIO_NUM && DETECT_INTER_NUM(s_sIrqInfo[i].m_u8GpioNum, u32Val))
        {
            s_sIrqInfo[i].m_pfnIrqCallback();
        }
    }
}
