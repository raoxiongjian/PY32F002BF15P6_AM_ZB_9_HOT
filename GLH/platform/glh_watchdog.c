/*
* Copyright (c) 2022, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_watchdog.c
* 文件标识：
* 摘 要：
*   看门狗
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2022年6月22日
*/
#include "main.h"
#include "glh_watchdog.h"


#ifdef SUPPORT_WD
static IWDG_HandleTypeDef   IwdgHandle;

void GLHWD_Init(void)
{
    /*##-3- Configure & Start the IWDG peripheral #########################################*/
    IwdgHandle.Instance = IWDG;                     //选择IWDG
    IwdgHandle.Init.Prescaler = IWDG_PRESCALER_32;  //配置32分频
    IwdgHandle.Init.Reload = (1000);                //IWDG计数器重装载值为1000，1s
}

void GLHWD_Start(void)
{

}

void GLHWD_Stop(void)
{

}

void GLHWD_Clear(void)
{
    /*喂狗*/
    if (HAL_IWDG_Refresh(&IwdgHandle) != HAL_OK)
    {
      Error_Handler();
    }
}
#endif
