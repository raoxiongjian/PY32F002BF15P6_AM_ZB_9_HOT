#ifndef __1635_DRIVER_H__
#define __1635_DRIVER_H__
#include "glh_typedef.h"

#define delay_us GLHDL_DelayUs

#define  SDA     P01

#define  SDA_1   P01=1               
#define  SDA_0   P01=0               

#define  SCK_1   P00=1
#define  SCK_0   P00=0

#define  SDA_IN  P01F = INPUT|PU_EN
#define  SDA_OUT P01F = OUTPUT;

void GLHTM1651_Init(void);
void GLHTM1651_SmgDisplay(uint8 *dat, uint8 length, uint8 led_brightness);
void GLHTM1651_ReadKey(uint8 *pu8Buff);
#endif

