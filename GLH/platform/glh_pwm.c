/*
* Copyright (c) 2023, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_pwm.h
* 文件标识：
* 摘 要：
*     通过定时器产生pwm
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2023年7月4日
*/
#include "main.h"
#include "glh_pwm.h"

#ifdef SUPPORT_PWM

#define DUTY_MAX   200

void GLHPWM_Init(uint8 u8Ch)
{
    LL_GPIO_InitTypeDef TIM1CH1MapInit= {0};
    LL_TIM_OC_InitTypeDef TIM_OC_Initstruct ={0};
    
    /* 使能TIM1时钟 */
    LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_TIM1);
    
    if(u8Ch & E_PWM_CH1)
    {
        /* 使能GPIOA时钟 */
        LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOA);
        
        /*配置PA5为TIM1_CH1*/
        TIM1CH1MapInit.Pin        = LL_GPIO_PIN_5;
        TIM1CH1MapInit.Mode       = LL_GPIO_MODE_ALTERNATE;
        TIM1CH1MapInit.Alternate  = LL_GPIO_AF_2; 
        LL_GPIO_Init(GPIOA,&TIM1CH1MapInit);
    }
    
    if(u8Ch & E_PWM_CH3)
    {
        /* 使能GPIOB时钟 */  
        LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOB);
        
        TIM1CH1MapInit.Pin        = LL_GPIO_PIN_2;
        TIM1CH1MapInit.Mode       = LL_GPIO_MODE_ALTERNATE;
        TIM1CH1MapInit.Alternate  = LL_GPIO_AF_3; 
        LL_GPIO_Init(GPIOB,&TIM1CH1MapInit);
    }
    
    /*配置PWM通道*/
    TIM_OC_Initstruct.OCMode        = LL_TIM_OCMODE_PWM2;       /* 通道模式：PWM2       */
    TIM_OC_Initstruct.OCState       = LL_TIM_OCSTATE_ENABLE;    /* 通道状态：开启       */
    TIM_OC_Initstruct.OCPolarity    = LL_TIM_OCPOLARITY_LOW;   /* 通道有效极性：高电平 */
    TIM_OC_Initstruct.OCIdleState   = LL_TIM_OCIDLESTATE_LOW;   /* 通道空闲极性：低电平 */
    /*通道1比较值:*/
    TIM_OC_Initstruct.CompareValue  = 0;
    /*配置通道1*/
    LL_TIM_OC_Init(TIM1,LL_TIM_CHANNEL_CH1,&TIM_OC_Initstruct);
    /*通道3比较值:*/
    TIM_OC_Initstruct.CompareValue  = 0;
    /*配置通道3*/
    LL_TIM_OC_Init(TIM1,LL_TIM_CHANNEL_CH3,&TIM_OC_Initstruct);
    
    /*配置TIM1*/
    LL_TIM_InitTypeDef TIM1CountInit = {0};
  
    TIM1CountInit.ClockDivision       = LL_TIM_CLOCKDIVISION_DIV1;/* 不分频             */
    TIM1CountInit.CounterMode         = LL_TIM_COUNTERMODE_UP;    /* 计数模式：向上计数 */
    TIM1CountInit.Prescaler           = 120-1;                   /* 时钟预分频：2400   */
    TIM1CountInit.Autoreload          = 200-1;                   /* 自动重装载值：1000 */
    TIM1CountInit.RepetitionCounter   = 0;                        /* 重复计数值：0      */
  
    /*初始化TIM1*/
    LL_TIM_Init(TIM1,&TIM1CountInit);
  
    /*主输出使能*/
    LL_TIM_EnableAllOutputs(TIM1);

    /*使能TIM1计数器*/
    LL_TIM_EnableCounter(TIM1);
}

void GLHPWM_Set(GLHPWM_CHANNEL_E eCh, uint32 u32Duty)
{
    if(eCh & E_PWM_CH1)
    {
        LL_TIM_OC_SetCompareCH1(TIM1, u32Duty);
    }
    else if(eCh & E_PWM_CH3)
    {
        LL_TIM_OC_SetCompareCH3(TIM1, u32Duty);
    }
}
#endif
