/*
* Copyright (c) 2022, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_ssd1309.h
* 文件标识：
* 摘 要：
*    
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2022年4月22日
*/

#ifndef __GLH_SSD1309_H
#define __GLH_SSD1309_H

#include "glh_typedef.h"

#define u8 uint8
#define u16 uint16
#define u32 uint32
 
//OLED控制用函数
void delay_ms(unsigned int ms);
void OLED_ColorTurn(u8 i);
void OLED_DisplayTurn(u8 i);
void OLED_WR_Byte(u8 dat,u8 cmd);
void OLED_Set_Pos(u8 x, u8 y);
void OLED_Display_On(void);
void OLED_Display_Off(void);
void OLED_Clear(void);
void OLED_ShowChar(u8 x,u8 y,u8 chr,u8 sizey);
u32 oled_pow(u8 m,u8 n);
void OLED_ShowNum(u8 x,u8 y,u32 num,u8 len,u8 sizey);
void OLED_ShowString(u8 x,u8 y,u8 *chr,u8 sizey);
void OLED_ShowChinese(u8 x,u8 y,u8 no,u8 sizey);
void OLED_DrawBMP(u8 x,u8 y,u8 sizex, u8 sizey,u8 BMP[]);
void OLED_Init(void);
void OLED_SetInvertedDisplay(BOOL bIsInvertedDisplay);

#endif  