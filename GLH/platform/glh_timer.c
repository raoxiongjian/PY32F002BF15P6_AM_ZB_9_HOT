/*
* Copyright (c) 2020, 深圳市君同科技有限公司
* All rights reserved.
*
* 文件名称：jt_timer.c
* 文件标识：
* 摘 要： 
*     定时模块
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2020年10月9日
*/
#include "jt_timer.h"

#define MAX_TIMER_NUM    4

static JTTMR_Callback_F s_apfnCallback[MAX_TIMER_NUM] = {0};

void JTTMR_Init(uint8 u8TimerNum, uint32 u32Us, JTTMR_Callback_F pfnCallback)
{
    if (u8TimerNum >= MAX_TIMER_NUM || u32Us < 4 || pfnCallback == NULL)
    {
        JTDEBUG(ERROR_LEVEL, "JTTMR_Init Error\n");
        return;
    }

    __write_hw_reg32(CPR_CTLAPBCLKEN_GRCTL,0x80008); //TIMER_PCLK 时钟使能
    __write_hw_reg32(CPR_TIMER_CLK_CTL(u8TimerNum),0x0F);//TIMERx_CLK 时钟控制寄存器 mclk_in(32MHz)/2*(0x0F + 0x1)
    __write_hw_reg32(TIMERx_TCR(u8TimerNum),0x0);//不屏蔽定时器中断，不使能定时器timer_num，
    __write_hw_reg32(TIMERx_TCR(u8TimerNum),0x2);//设置定时器工作在用户定义计数模式
    __write_hw_reg32(TIMERx_TLC(u8TimerNum),u32Us);//载入计数器计数初值(32bits),该值应大于等于 0x4
    __write_hw_reg32(TIMERx_TCR(u8TimerNum),0x3);//使能定时器timer_num
    NVIC_EnableIRQ((IRQn_Type)(TIMER0_IRQn+u8TimerNum));

    s_apfnCallback[u8TimerNum] = pfnCallback;
}


void JTTMR_DeInit(uint8 u8TimerNum)
{
    if (u8TimerNum >= MAX_TIMER_NUM)
    {
        JTDEBUG(ERROR_LEVEL, "JTTMR_DeInit Error\n");
        return;
    }

    __write_hw_reg32(TIMERx_TCR(u8TimerNum),0x0);//停止对应的定时器
}

void TIMER0_Handler(void)
{
    if (s_apfnCallback[0] != NULL)
    {
        s_apfnCallback[0]();
    }
}

void TIMER1_Handler(void)
{
    if (s_apfnCallback[1] != NULL)
    {
        s_apfnCallback[1]();
    }
}

void TIMER2_Handler(void)
{
    if (s_apfnCallback[2] != NULL)
    {
        s_apfnCallback[2]();
    }
}

void TIMER3_Handler(void)
{
    if (s_apfnCallback[3] != NULL)
    {
        s_apfnCallback[3]();
    }
}