/*
* Copyright (c) 2022, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_over_current_protect.c
* 文件标识：
* 摘 要：
*     过流/短路保护
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2022年11月29日
*/
#include "main.h"
#include "glh_over_current_protect.h"
#include "glh_sys_tick.h"
#include "glh_pwm.h"

#ifdef SUPPORT_OVER_CURRENT_PROTECT

#define OVER_CURRENT_PROTECT_TIME     5000

volatile BOOL s_bIsOverCurrent = FALSE;
static uint32 s_u32OverCurrentStartTime = 0;
COMP_HandleTypeDef  hcomp1;

void GLHOCP_Init(void)
{
    __HAL_RCC_GPIOA_CLK_ENABLE();                 //使能GPIOA时钟
    __HAL_RCC_COMP1_CLK_ENABLE();                 //使能COMP1时钟
    //GPIOPA1配置为模拟输入
    GPIO_InitTypeDef COMPINPUT;
    COMPINPUT.Pin = GPIO_PIN_0 | GPIO_PIN_1;
    COMPINPUT.Mode = GPIO_MODE_ANALOG;            //模拟模式
    COMPINPUT.Speed = GPIO_SPEED_FREQ_HIGH;
    COMPINPUT.Pull = GPIO_NOPULL;                //下拉
    HAL_GPIO_Init(GPIOA,  &COMPINPUT);            //GPIO初始化

    HAL_NVIC_EnableIRQ(ADC_COMP_IRQn);            //使能COMP中断
    HAL_NVIC_SetPriority(ADC_COMP_IRQn, 0, 0);
    
    hcomp1.Instance = COMP1;                                              //选择COM1
    hcomp1.Init.InputMinus      = COMP_INPUT_MINUS_IO3;                   //负输入为PA0
    hcomp1.Init.InputPlus       = COMP_INPUT_PLUS_IO3;                    //正输入选择为PA1
    hcomp1.Init.OutputPol       = COMP_OUTPUTPOL_NONINVERTED;             //COMP1极性选择为不反向
    hcomp1.Init.Mode            = COMP_POWERMODE_HIGHSPEED;               //COMP1功耗模式选择为High speed模式
    hcomp1.Init.Hysteresis      = COMP_HYSTERESIS_DISABLE;                //迟滞功能关闭
    hcomp1.Init.WindowMode      = COMP_WINDOWMODE_DISABLE;                //WIODOW关闭
    hcomp1.Init.TriggerMode     = COMP_TRIGGERMODE_IT_RISING;             //COMP1上升沿触发
    if (HAL_COMP_Init(&hcomp1) != HAL_OK)                                 //COMP1初始化
    {
        Error_Handler();
    }
    
    HAL_COMP_Start(&hcomp1);                                              //COMP1启动 
}

void GLHOCP_MainThread(void)
{
    if(s_bIsOverCurrent == TRUE)       //处于保护状态
    {
        if(GulSystickCount - s_u32OverCurrentStartTime > OVER_CURRENT_PROTECT_TIME)    //超过解除保护的时间
        {
            s_bIsOverCurrent = FALSE;
            AllPwmChOutPut();
        }
    }
}

void ADC_COMP_IRQHandler(void)
{
    HAL_COMP_IRQHandler(&hcomp1);
}

/********************************************************************************************************
**函数信息 ：HAL_COMP_TriggerCallback(COMP_HandleTypeDef *hcomp)
**功能描述 ：COMP执行函数，每进一次中断，翻转PA11
**输入参数 ：COMP_HandleTypeDef *hcomp
**输出参数 ：
**    备注 ：
********************************************************************************************************/
void HAL_COMP_TriggerCallback(COMP_HandleTypeDef *hcomp)
{
    AllPwmChNoOutPut();
    
    s_bIsOverCurrent = TRUE;
    s_u32OverCurrentStartTime = GulSystickCount;
}
#endif

