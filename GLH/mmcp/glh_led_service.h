/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_sc_service.h
* 文件标识：
* 摘 要：
*   主要逻辑层的实现
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年6月18日
*/
#ifndef __GLH_SC_SERVICE_H__
#define __GLH_SC_SERVICE_H__
#include "glh_typedef.h"
#include "glh_led_mode.h"

void GLHSCS_Init(void);
void GLHSCS_MainThread(void);
void GLHSCS_SetRgbBuff(uint8 u8Index, RGBW_S sRgb);
void GLHSCS_SetRgbModeStatic(void);
void GLHSCS_SetRgbModeFlash(uint8 u8ColorNum);
void GLHSCS_SetRgbModeFade(uint8 u8ColorNum);
void GLHSCS_SetRgbModeJump(uint8 u8ColorNum);
void GLHSCS_SetRgbModeOff(void);
void GLHSCS_SetRgbModeOn(void);
void GLHSCS_SetWhiteModeOff(void);
void GLHSCS_SetWhiteModeOn(void);
void GLHSCS_SetWhiteBrightNess(uint8 u8WhiteBrightNess);
void GLHSCS_SetWhiteTemprature(uint8 u8WhiteTemprature);
void GLHSCS_SetSpeed(uint8 u8Speed);

#endif //__JT_SC_SERVICE_H__
