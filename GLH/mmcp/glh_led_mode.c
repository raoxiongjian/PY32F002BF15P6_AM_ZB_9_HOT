/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：jt_led_mode.c
* 文件标识：
* 摘 要：
*    灯条模式的具体实现
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年7月5日
*/
#include "glh_led_mode.h"
#include "glh_rgb.h"
#include "glh_sys_tick.h"

#define     STATIC_DYNAMIC_GRAD    1
#define     DYNAMIC_GRAD         1       //动态模式的变化梯度
#define     SPEED_PARAM    1
#define     iResetCompleteFlag()          s_u8CompleteFlag = 0
#define     iSetCompleteFlag()            s_u8CompleteFlag = 1
#define     iIsComplete()                 (s_u8CompleteFlag == 1)
#define     iSetRgbw(R, G, B, W)   GLHRGB_SetColor(R, G, B, W)
#define     MAX_BN     255
#define     JIANBIAN_MODE_SPEED     ((MAX_SPEED - s_u8Speed)*SPEED_PARAM)
#define     TIAOBIAN_MODE_SPEED      ((JIANBIAN_MODE_SPEED+2)*90)
#define     STATIC_MODE_ACTION_TIME   3

typedef void (* MODE_SHADE_F)(void);     //渲染线程

static MODE_SHADE_F XDATA s_pfnModeShade = NULL;       //彩光模式线程

static uint8 XDATA s_u8CurrColorNum = 0;         //总共设置了几个颜色
static uint8 XDATA s_u8ColorIndex = 0;           //当前变化到了第几个颜色
static uint8 XDATA s_u8CurrR = 0, s_u8CurrG = 0, s_u8CurrB = 0, s_u8CurrW = 0;  //当前的RGB值
static uint8 XDATA s_u8CurrBn = 0;
static RGBW_S XDATA s_sRgbw[COLOR_NUM_MAX] = {0};
static uint8 XDATA s_u8Flag = 0, s_u8Flag1 = 0, s_u8Flag2 = 0;
static uint8 XDATA s_u8CompleteFlag = 0; //动作是否完成
static uint8 XDATA s_u8Speed = 80;
static uint32 XDATA u32LastRgbTime = 0, u32LastWhiteTime = 0;
static uint8 XDATA s_u8Bn = 255;  //只针对静态模式
static uint8 s_u8Cnt = 0;

static void iResetParams(void)
{
	s_u8Flag = 0;
	s_u8Flag1 = 0;
	s_u8Flag2 = 0;
	s_u8Cnt = 0;
    s_u8ColorIndex = 0;
	s_u8CurrR = 0;
	s_u8CurrG = 0;
	s_u8CurrB = 0;
	s_u8CurrW = 0;
	s_u8CurrBn = 0;
    iResetCompleteFlag();
}

void GLHLM_Init(void)
{
    GLHRGB_Init();
    iResetParams();
}

void GLHLM_RenderProc(void)
{
    if (s_pfnModeShade != NULL)
    {
        s_pfnModeShade();
    }
}

BOOL GLHLM_GetModeIsComplete(void)
{
    return iIsComplete();
}

static void iCheckIsComplete(void)
{
    s_u8ColorIndex++;
    if (s_u8ColorIndex >= s_u8CurrColorNum)
    {
        s_u8ColorIndex = 0;
        iSetCompleteFlag();   //一次动作执行完成
    }
    else
    {
        iResetCompleteFlag();
    }
}

void GLHLM_SetColorBuff(uint8 u8Index, uint8 u8R, uint8 u8G, uint8 u8B, uint8 u8W)
{
    if((u8Index >= COLOR_NUM_MAX))
    {
        return;
    }
    
    s_sRgbw[u8Index].u8r = u8R;
    s_sRgbw[u8Index].u8g = u8G;
    s_sRgbw[u8Index].u8b = u8B;
	s_sRgbw[u8Index].u8w = u8W;
}

void iStatic(void)
{
    if(GulSystickCount - u32LastRgbTime < STATIC_MODE_ACTION_TIME)
    {
        return;
    }
    u32LastRgbTime = GulSystickCount;
    
    if(s_u8CurrR > s_sRgbw[0].u8r)
    {
        if(s_u8CurrR - s_sRgbw[0].u8r >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrR -= STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrR = s_sRgbw[0].u8r;
        }
    }
    else
    {
        if(s_sRgbw[0].u8r - s_u8CurrR >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrR += STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrR = s_sRgbw[0].u8r;
        }
    }
    
    if(s_u8CurrG > s_sRgbw[0].u8g)
    {
        if(s_u8CurrG - s_sRgbw[0].u8g >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrG -= STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrG = s_sRgbw[0].u8g;
        }
    }
    else
    {
        if(s_sRgbw[0].u8g - s_u8CurrG >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrG += STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrG = s_sRgbw[0].u8g;
        }
    }
    
    if(s_u8CurrB > s_sRgbw[0].u8b)
    {
        if(s_u8CurrB - s_sRgbw[0].u8b >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrB -= STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrB = s_sRgbw[0].u8b;
        }
    }
    else
    {
        if(s_sRgbw[0].u8b - s_u8CurrB >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrB += STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrB = s_sRgbw[0].u8b;
        }
    }
	
    if(s_u8CurrW > s_sRgbw[0].u8w)
    {
        if(s_u8CurrW - s_sRgbw[0].u8w >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrW -=STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrW = s_sRgbw[0].u8w;
        }
    }
    else
    {
        if(s_sRgbw[0].u8w - s_u8CurrW >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrW += STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrW = s_sRgbw[0].u8w;
        }
    }

    iSetRgbw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrW);	
}

/*
    功能: 设置为静态常亮模式,颜色渐变
    输入: 无
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_StaticMode(void)
{
	s_pfnModeShade = iStatic;
    u32LastRgbTime = GulSystickCount - STATIC_MODE_ACTION_TIME;  //为了使模式能够立即执行
}

/*
    功能: 设置为静态常亮模式,立即生效，不用渐变
    输入: 无
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_StaticModeImmediately(void)
{
	iResetParams();
    
    s_u8CurrR = s_sRgbw[0].u8r;
    s_u8CurrG = s_sRgbw[0].u8g;
    s_u8CurrB = s_sRgbw[0].u8b;
    s_u8CurrW = s_sRgbw[0].u8w;
    
	iSetRgbw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrW);
	s_pfnModeShade = NULL;
}

static void iFlash(void)
{
    if(GulSystickCount - u32LastRgbTime < TIAOBIAN_MODE_SPEED)
    {
        return;
    }
    u32LastRgbTime = GulSystickCount;
    
    if(s_u8Flag)
    {
        s_u8CurrR = 0;
        s_u8CurrG = 0;
        s_u8CurrB = 0;
        s_u8CurrW = 0;
        iSetRgbw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrW);
        iCheckIsComplete();      
    }
    else
    {
        s_u8CurrR = s_sRgbw[s_u8ColorIndex].u8r;
        s_u8CurrG = s_sRgbw[s_u8ColorIndex].u8g;
        s_u8CurrB = s_sRgbw[s_u8ColorIndex].u8b;
        s_u8CurrW = s_sRgbw[s_u8ColorIndex].u8w;
        iSetRgbw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrW);
    }  
    
    s_u8Flag = !s_u8Flag;
}

/*
    功能: 设置为闪烁模式
    输入: u8ColorNum    设置闪烁的颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_FlashMode(uint8 u8ColorNum)
{
    if ((u8ColorNum == 0) || (u8ColorNum > COLOR_NUM_MAX))
    {
        return;
    }
    
    iResetParams();
    s_u8CurrColorNum = u8ColorNum;
 
    s_pfnModeShade = iFlash;
    
    u32LastRgbTime = GulSystickCount - TIAOBIAN_MODE_SPEED;  //为了使模式能够立即执行
}

static void iBth(void)
{
    if(GulSystickCount - u32LastRgbTime < JIANBIAN_MODE_SPEED)
    {
        return;
    }
    u32LastRgbTime = GulSystickCount;
    
    if(s_u8CurrColorNum == 0)
    {
        return;
    }
	
    if (s_u8Flag)
    {
		if(s_u8CurrBn - DYNAMIC_GRAD > 0)
		{
            s_u8CurrBn -= DYNAMIC_GRAD;
		}
		else
	    {
		    s_u8CurrBn = 0;
		}

        s_u8CurrR = s_sRgbw[s_u8ColorIndex].u8r*s_u8CurrBn/255;
        s_u8CurrG = s_sRgbw[s_u8ColorIndex].u8g*s_u8CurrBn/255;
        s_u8CurrB = s_sRgbw[s_u8ColorIndex].u8b*s_u8CurrBn/255;
        s_u8CurrW = s_sRgbw[s_u8ColorIndex].u8w*s_u8CurrBn/255;		
		iSetRgbw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrW);

        if (s_u8CurrBn == 0)
        {
            s_u8Flag = 0;

            iCheckIsComplete();
        }
    }
    else
    {
		if(s_u8CurrBn + DYNAMIC_GRAD <= MAX_BN)
		{
			s_u8CurrBn += DYNAMIC_GRAD;	
		}
		else
	    {
			s_u8CurrBn = MAX_BN;
		}

        s_u8CurrR = s_sRgbw[s_u8ColorIndex].u8r*s_u8CurrBn/255;
        s_u8CurrG = s_sRgbw[s_u8ColorIndex].u8g*s_u8CurrBn/255;
        s_u8CurrB = s_sRgbw[s_u8ColorIndex].u8b*s_u8CurrBn/255;
        s_u8CurrW = s_sRgbw[s_u8ColorIndex].u8w*s_u8CurrBn/255;		
		iSetRgbw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrW);

        if (s_u8CurrBn == MAX_BN)
        {
            s_u8Flag = 1;

        }
    }
}

/*
    功能: 设置为呼吸模式
    输入: u8ColorNum    设置呼吸的颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_BreathMode(uint8 u8ColorNum)
{
    if ((u8ColorNum == 0) || (u8ColorNum > COLOR_NUM_MAX))
    {
        return;
    }
    
    iResetParams();
    
    s_u8CurrColorNum = u8ColorNum;
    
    s_pfnModeShade = iBth;
    
    u32LastRgbTime = GulSystickCount - JIANBIAN_MODE_SPEED;  //为了使模式能够立即执行
}

static void iFade(void)
{
    if(GulSystickCount - u32LastRgbTime < JIANBIAN_MODE_SPEED)
    {
        return;
    }
    u32LastRgbTime = GulSystickCount;
    
    if(s_u8CurrColorNum == 0)
    {
        return;
    }

    if(s_u8CurrR > s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8r)
    {
        if(s_u8CurrR - s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8r >= DYNAMIC_GRAD)
        {
            s_u8CurrR -= DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrR = s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8r;
        }
    }
    else
    {
        if(s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8r - s_u8CurrR >= DYNAMIC_GRAD)
        {
            s_u8CurrR += DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrR = s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8r;
        }
    }
    
    if(s_u8CurrG > s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8g)
    {
        if(s_u8CurrG - s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8g >= DYNAMIC_GRAD)
        {
            s_u8CurrG -= DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrG = s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8g;
        }
    }
    else
    {
        if(s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8g - s_u8CurrG >= DYNAMIC_GRAD)
        {
            s_u8CurrG += DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrG = s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8g;
        }
    }
    
    if(s_u8CurrB > s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8b)
    {
        if(s_u8CurrB - s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8b >= DYNAMIC_GRAD)
        {
            s_u8CurrB -= DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrB = s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8b;
        }
    }
    else
    {
        if(s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8b - s_u8CurrB >= DYNAMIC_GRAD)
        {
            s_u8CurrB += DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrB = s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8b;
        }
    }
	
    if(s_u8CurrW > s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8w)
    {
        if(s_u8CurrW - s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8w >= DYNAMIC_GRAD)
        {
            s_u8CurrW -= DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrW = s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8w;
        }
    }
    else
    {
        if(s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8w - s_u8CurrW >= DYNAMIC_GRAD)
        {
            s_u8CurrW += DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrW = s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8w;
        }
    }
    
    iSetRgbw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrW);
	
	if((s_u8CurrR == s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8r) && (s_u8CurrG == s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8g)\
        &&(s_u8CurrB == s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8b) &&(s_u8CurrW == s_sRgbw[(s_u8ColorIndex + 1) % s_u8CurrColorNum].u8w))
	{
		iCheckIsComplete();
	} 
}

/*
    功能: 设置为渐变模式
    输入: u8ColorNum    设置渐变的颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_FadeMode(uint8 u8ColorNum)
{
    if ((u8ColorNum == 0) || (u8ColorNum > COLOR_NUM_MAX))
    {
        return;
    }
    
    iResetParams();
	s_u8ColorIndex = u8ColorNum - 1;
    
    s_u8CurrColorNum = u8ColorNum;
    
    s_pfnModeShade = iFade;
    
    u32LastRgbTime = GulSystickCount - JIANBIAN_MODE_SPEED;  //为了使模式能够立即执行
}

static void iOneBreathAndThreeJump(void)
{
    if(s_u8Flag == 0)
	{
		if(GulSystickCount - u32LastRgbTime < JIANBIAN_MODE_SPEED)
		{
			return;
		}
		u32LastRgbTime = GulSystickCount;
    
		if(s_u8CurrColorNum == 0)
		{
			return;
		}
	
		if (s_u8Flag1)
		{
			if(s_u8CurrBn - DYNAMIC_GRAD > 0)
			{
				s_u8CurrBn -= DYNAMIC_GRAD;
			}
			else
			{
				s_u8CurrBn = 0;
			}

            s_u8CurrR = s_sRgbw[s_u8ColorIndex].u8r*s_u8CurrBn/255;
            s_u8CurrG = s_sRgbw[s_u8ColorIndex].u8g*s_u8CurrBn/255;
            s_u8CurrB = s_sRgbw[s_u8ColorIndex].u8b*s_u8CurrBn/255;
            s_u8CurrW = s_sRgbw[s_u8ColorIndex].u8w*s_u8CurrBn/255;		
            iSetRgbw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrW);            

			if (s_u8CurrBn == 0)
			{
				s_u8Flag = 1;
                s_u8Flag1 = 0;
			}
		}
		else
		{
			if(s_u8CurrBn + DYNAMIC_GRAD <= MAX_BN)
			{
				s_u8CurrBn += DYNAMIC_GRAD;	
			}
			else
			{
				s_u8CurrBn = MAX_BN;
			}

            s_u8CurrR = s_sRgbw[s_u8ColorIndex].u8r*s_u8CurrBn/255;
            s_u8CurrG = s_sRgbw[s_u8ColorIndex].u8g*s_u8CurrBn/255;
            s_u8CurrB = s_sRgbw[s_u8ColorIndex].u8b*s_u8CurrBn/255;
            s_u8CurrW = s_sRgbw[s_u8ColorIndex].u8w*s_u8CurrBn/255;		
            iSetRgbw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrW);       

			if (s_u8CurrBn == MAX_BN)
			{
				s_u8Flag1 = 1;
			}
		}	
	}
	else
    {
		if(GulSystickCount - u32LastRgbTime < TIAOBIAN_MODE_SPEED)
		{
			return;
		}
		u32LastRgbTime = GulSystickCount;
    
		if(s_u8Flag2)
		{
			iSetRgbw(0, 0, 0, 0);
		}
		else
		{
			s_u8Cnt++;
			if(s_u8Cnt < 4)
			{
                s_u8CurrR = s_sRgbw[s_u8ColorIndex].u8r;
                s_u8CurrG = s_sRgbw[s_u8ColorIndex].u8g;
                s_u8CurrB = s_sRgbw[s_u8ColorIndex].u8b;
                s_u8CurrW = s_sRgbw[s_u8ColorIndex].u8w;		
                iSetRgbw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrW);    
			}
            else
			{
				s_u8Cnt = 0;
				s_u8Flag = 0;
				s_u8Flag1 = 0;
				s_u8Flag2 = 0;
				s_u8CurrBn = 0;
                iCheckIsComplete();  
			}    
		}  
    
		s_u8Flag2 = !s_u8Flag2;	
	}
}

/*
    功能: 设置为一呼吸三跳变模式
    输入: u8ColorNum    设置一呼吸三跳变颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_OneBthAndThreeJumpMode(uint8 u8ColorNum)
{
    if ((u8ColorNum == 0) || (u8ColorNum > COLOR_NUM_MAX))
    {
        return;
    }
    
    iResetParams();
    
    s_u8CurrColorNum = u8ColorNum;
    
    s_pfnModeShade = iOneBreathAndThreeJump;
    u32LastRgbTime = GulSystickCount - TIAOBIAN_MODE_SPEED;  //为了使模式能够立即执行
}

static void iJump(void)
{
    if(GulSystickCount - u32LastRgbTime < TIAOBIAN_MODE_SPEED)
    {
        return;
    }
	u32LastRgbTime = GulSystickCount;

    if (s_u8ColorIndex >= s_u8CurrColorNum)
    {
        s_u8ColorIndex = 0;
        iSetCompleteFlag();   //一次动作执行完成
		u32LastRgbTime = GulSystickCount - TIAOBIAN_MODE_SPEED;
		return;
    }
    else
    {
        iResetCompleteFlag();
    }

    s_u8CurrR = s_sRgbw[s_u8ColorIndex].u8r;
    s_u8CurrG = s_sRgbw[s_u8ColorIndex].u8g;
    s_u8CurrB = s_sRgbw[s_u8ColorIndex].u8b;
    s_u8CurrW = s_sRgbw[s_u8ColorIndex].u8w;		
    iSetRgbw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrW);    	

	s_u8ColorIndex++;
}

/*
    功能: 设置为跳变模式
    输入: u8ColorNum    设置跳变的颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_JumpMode(uint8 u8ColorNum)
{
    if ((u8ColorNum == 0) || (u8ColorNum > COLOR_NUM_MAX))
    {
        return;
    }
    
    iResetParams();
    
    s_u8CurrColorNum = u8ColorNum;
    
    s_pfnModeShade = iJump;
    u32LastRgbTime = GulSystickCount - TIAOBIAN_MODE_SPEED;  //为了使模式能够立即执行
}


/*
    功能: 设置速度
    输入: u8Speed    速度参数
    输出: 无
    说明：
*/
void GLHLM_SetSpeed(uint8 u8Speed)
{
	if(u8Speed > MAX_SPEED)
    {
		s_u8Speed = MAX_SPEED;
	}
	
	if(u8Speed < MIN_SPEED)
    {
		s_u8Speed = MIN_SPEED;
	}	
	
    s_u8Speed = u8Speed;
}

void GLHLM_SetBn(uint8 u8bn)
{
    GLHRGB_SetBn(u8bn);
}