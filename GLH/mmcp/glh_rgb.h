/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_rgb.h
* 文件标识：
* 摘 要：
*   rgb颜色驱动实现模块
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2022年5月12日
*/
#ifndef __GLH_RGB_H__
#define __GLHT_RGB_H__
#include "glh_typedef.h"
#include "glh_pwm.h"

/**
    @功能: RGB初始胡
    @参数: 
    @返回: 
*/
void GLHRGB_Init(void);


/**
    @功能: 设置颜色值
    @参数: 
            u8R[in]: 红色分量
            u8G[in]: 绿色分量
            u8B[in]: 蓝色分量
            u8W[in]: 蓝色分量
    @返回: 
    @备注: 各分量的值会与亮度值和开关机进度值参与计算后输出
*/
void GLHRGB_SetColor(uint8 u8R, uint8 u8G, uint8 u8B, uint8 u8W);
void GLHRGB_SetColor_W(uint8 u8W);


/**
    @功能: 设置亮度值
    @参数: 
            u8R[in]: 亮度值
    @返回: 
    @备注: 各分量的值会和参与计算后输出
*/
void GLHRGB_SetBn(uint8 u8Bn);


/**
    @功能: 设置开关机进度值
    @参数: 
            u8OnOffStateValue[in]: 开关机进度值
    @返回: 
    @备注: 各分量的值会和亮度参与计算后输出
*/
void GLHRGB_SetOnOffValue(uint8 u8OnOffStateValue);


/**
    @功能: 设置是否需要对数调光
    @参数: 
            bIsSupportLogarithmicDimming：TRUE 表示需要对数调光， FALSE表示不需要对数调光
    @返回: 
    @备注: 各分量的值会和亮度参与计算后输出
*/
void GLHRGB_SetLogarithmicDimming(BOOL bIsSupportLogarithmicDimming);

#endif

