/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_led_mode.h
* 文件标识：
* 摘 要：
*    灯条模式的具体实现
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年6月18日
*/
#ifndef __GLH_LED_MODE_H__
#define __GLH_LED_MODE_H__
#include "glh_typedef.h"
#include "glh_rgb.h"

#ifndef COLOR_NUM_MAX
#define COLOR_NUM_MAX 8
#endif

#ifndef H_MAX 
#define H_MAX                    360
#endif

#ifndef S_MAX 
#define S_MAX                    1000.0
#endif

#ifndef V_MAX 
#define V_MAX                    1000.0
#endif

#ifndef W_MAX 
#define W_MAX                    1000
#endif

#ifndef MAX_SPEED
#define MAX_SPEED                100
#endif

#ifndef MIN_SPEED
#define MIN_SPEED                1
#endif

typedef struct _RGBW_S
{
    uint8 u8r;
    uint8 u8g;
    uint8 u8b;
	uint8 u8w;
}RGBW_S;

void GLHLM_Init(void);
void GLHLM_SetColorBuff(uint8 u8Index, uint8 u8R, uint8 u8G, uint8 u8B, uint8 u8W);
void GLHLM_RenderProc(void);
void GLHLM_StaticModeImmediately(void);
void GLHLM_StaticMode(void);
void GLHLM_FlashMode(uint8 u8ColorNum);
void GLHLM_BreathMode(uint8 u8ColorNum);
void GLHLM_FadeMode(uint8 u8ColorNum);
void GLHLM_JumpMode(uint8 u8ColorNum);
void GLHLM_OneBthAndThreeJumpMode(uint8 u8ColorNum);
void GLHLM_SetSpeed(uint8 u8Speed);
BOOL GLHLM_GetModeIsComplete(void);
void GLHLM_SetBn(uint8 u8bn);

#endif //__GLH_LED_MODE_H__



