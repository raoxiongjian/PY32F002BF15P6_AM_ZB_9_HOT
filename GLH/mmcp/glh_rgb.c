/*
* Copyright (c) 2022, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_rgb.c
* 文件标识：
* 摘 要：
*   rgb颜色驱动实现模块
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2021年7月5日
*/
#include "glh_typedef.h"
#include "glh_rgb.h"
#include "math.h"

#define GAMMA    1.6
#define LOG_DIMMER_COLOR_MIN   5
#define SPECIAL_HANDLE_END     10

static uint8 s_u8Bn = 255;
static uint8 s_u8OnOffStateValue = 255;            //用来处理开关机淡入淡出效果
static uint8 s_u8LastR = 0, s_u8LastG = 0, s_u8LastB = 0, s_u8LastW = 0;
static BOOL s_bIsLogarithmicDimming = FALSE;            //是否需要对数调光

void GLHRGB_Init(void)
{
    GLHPWM_Init(PWM_CHANNEL_R | PWM_CHANNEL_G | PWM_CHANNEL_B | PWM_CHANNEL_W);
    GLHRGB_SetColor(0, 0, 0, 0);
}


void GLHRGB_SetColor(uint8 u8R, uint8 u8G, uint8 u8B, uint8 u8W)
{
	s_u8LastR = u8R;
	s_u8LastG = u8G;
	s_u8LastB = u8B;
    u8W = 0;   //只是为了去掉编译时的警告，无任何实际意义
    
    if(s_bIsLogarithmicDimming == FALSE)
    {
        GLHPWM_Set(PWM_CHANNEL_R, (uint32)u8R*s_u8Bn*s_u8OnOffStateValue);
        GLHPWM_Set(PWM_CHANNEL_G, (uint32)u8G*s_u8Bn*s_u8OnOffStateValue);
        GLHPWM_Set(PWM_CHANNEL_B, (uint32)u8B*s_u8Bn*s_u8OnOffStateValue);
    }
    else
    {
        if((u8R > 0) && (u8R <= SPECIAL_HANDLE_END))
        {
            GLHPWM_Set(PWM_CHANNEL_R, (pow((float)(((float)LOG_DIMMER_COLOR_MIN + ((float)u8R - (float)LOG_DIMMER_COLOR_MIN)*((float)SPECIAL_HANDLE_END - (float)LOG_DIMMER_COLOR_MIN) /\
                        (float)SPECIAL_HANDLE_END) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX)*s_u8Bn*s_u8OnOffStateValue);
        }
        else
        {
            GLHPWM_Set(PWM_CHANNEL_R, ((pow((float)(((float)u8R) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX))*s_u8Bn*s_u8OnOffStateValue);
        }
        
        if((u8G > 0) && (u8G <= SPECIAL_HANDLE_END))
        {
            GLHPWM_Set(PWM_CHANNEL_G, (pow((float)(((float)LOG_DIMMER_COLOR_MIN + ((float)u8G - (float)LOG_DIMMER_COLOR_MIN)*((float)SPECIAL_HANDLE_END - (float)LOG_DIMMER_COLOR_MIN) /\
                        (float)SPECIAL_HANDLE_END) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX)*s_u8Bn*s_u8OnOffStateValue);
        }
        else
        {
            GLHPWM_Set(PWM_CHANNEL_G, ((pow((float)(((float)u8G) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX))*s_u8Bn*s_u8OnOffStateValue);
        }
        
        if((u8B > 0) && (u8B <= SPECIAL_HANDLE_END))
        {
            GLHPWM_Set(PWM_CHANNEL_B, (pow((float)(((float)LOG_DIMMER_COLOR_MIN + ((float)u8B - (float)LOG_DIMMER_COLOR_MIN)*((float)SPECIAL_HANDLE_END - (float)LOG_DIMMER_COLOR_MIN) /\
                        (float)SPECIAL_HANDLE_END) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX)*s_u8Bn*s_u8OnOffStateValue);
        }  
        else
        {
            GLHPWM_Set(PWM_CHANNEL_B, ((pow((float)(((float)u8B) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX))*s_u8Bn*s_u8OnOffStateValue); 
        }
    }
}

void GLHRGB_SetColor_W(uint8 u8W)
{
    s_u8LastW = u8W;
    
    if(s_bIsLogarithmicDimming == FALSE)
    {
        GLHPWM_Set(PWM_CHANNEL_W, (uint32)u8W*s_u8Bn*s_u8OnOffStateValue);
    }
    else
    {
        if((u8W > 0) && (u8W <= SPECIAL_HANDLE_END))
        {
            GLHPWM_Set(PWM_CHANNEL_W, (pow((float)(((float)LOG_DIMMER_COLOR_MIN + ((float)u8W - (float)LOG_DIMMER_COLOR_MIN)*((float)SPECIAL_HANDLE_END - (float)LOG_DIMMER_COLOR_MIN) /\
                        (float)SPECIAL_HANDLE_END) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX)*s_u8Bn*s_u8OnOffStateValue);
        }
        else
        {
            GLHPWM_Set(PWM_CHANNEL_W, ((pow((float)(((float)u8W) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX))*s_u8Bn*s_u8OnOffStateValue);
        }
    }
}


void GLHRGB_SetBn(uint8 u8Bn)
{
    s_u8Bn = u8Bn;
	
    if(s_bIsLogarithmicDimming == FALSE)
    {
        GLHPWM_Set(PWM_CHANNEL_R, (uint32)s_u8LastR*s_u8Bn*s_u8OnOffStateValue);
        GLHPWM_Set(PWM_CHANNEL_G, (uint32)s_u8LastG*s_u8Bn*s_u8OnOffStateValue);
        GLHPWM_Set(PWM_CHANNEL_B, (uint32)s_u8LastB*s_u8Bn*s_u8OnOffStateValue);
    }
    else
    {
        if((s_u8LastR > 0) && (s_u8LastR <= SPECIAL_HANDLE_END))
        {
            GLHPWM_Set(PWM_CHANNEL_R, (pow((float)(((float)LOG_DIMMER_COLOR_MIN + ((float)s_u8LastR - (float)LOG_DIMMER_COLOR_MIN)*((float)SPECIAL_HANDLE_END - (float)LOG_DIMMER_COLOR_MIN) /\
                        (float)SPECIAL_HANDLE_END) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX)*s_u8Bn*s_u8OnOffStateValue);
        }
        else
        {
            GLHPWM_Set(PWM_CHANNEL_R, ((pow((float)(((float)s_u8LastR) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX))*s_u8Bn*s_u8OnOffStateValue);
        }
        
        if((s_u8LastG > 0) && (s_u8LastG <= SPECIAL_HANDLE_END))
        {
            GLHPWM_Set(PWM_CHANNEL_G, (pow((float)(((float)LOG_DIMMER_COLOR_MIN + ((float)s_u8LastG - (float)LOG_DIMMER_COLOR_MIN)*((float)SPECIAL_HANDLE_END - (float)LOG_DIMMER_COLOR_MIN) /\
                        (float)SPECIAL_HANDLE_END) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX)*s_u8Bn*s_u8OnOffStateValue);
        }
        else
        {
            GLHPWM_Set(PWM_CHANNEL_G, ((pow((float)(((float)s_u8LastG) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX))*s_u8Bn*s_u8OnOffStateValue);
        }
        
        if((s_u8LastB > 0) && (s_u8LastB <= SPECIAL_HANDLE_END))
        {
            GLHPWM_Set(PWM_CHANNEL_B, (pow((float)(((float)LOG_DIMMER_COLOR_MIN + ((float)s_u8LastB - (float)LOG_DIMMER_COLOR_MIN)*((float)SPECIAL_HANDLE_END - (float)LOG_DIMMER_COLOR_MIN) /\
                        (float)SPECIAL_HANDLE_END) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX)*s_u8Bn*s_u8OnOffStateValue);
        }  
        else
        {
            GLHPWM_Set(PWM_CHANNEL_B, ((pow((float)(((float)s_u8LastB) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX))*s_u8Bn*s_u8OnOffStateValue); 
        }
    }
}

void GLHRGB_SetOnOffValue(uint8 u8OnOffStateValue)
{
    s_u8OnOffStateValue = u8OnOffStateValue;
	
    if(s_bIsLogarithmicDimming == FALSE)
    {
        GLHPWM_Set(PWM_CHANNEL_R, (uint32)s_u8LastR*s_u8Bn*s_u8OnOffStateValue);
        GLHPWM_Set(PWM_CHANNEL_G, (uint32)s_u8LastG*s_u8Bn*s_u8OnOffStateValue);
        GLHPWM_Set(PWM_CHANNEL_B, (uint32)s_u8LastB*s_u8Bn*s_u8OnOffStateValue);
    }
    else
    {
        if((s_u8LastR > 0) && (s_u8LastR <= SPECIAL_HANDLE_END))
        {
            GLHPWM_Set(PWM_CHANNEL_R, (pow((float)(((float)LOG_DIMMER_COLOR_MIN + ((float)s_u8LastR - (float)LOG_DIMMER_COLOR_MIN)*((float)SPECIAL_HANDLE_END - (float)LOG_DIMMER_COLOR_MIN) /\
                        (float)SPECIAL_HANDLE_END) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX)*s_u8Bn*s_u8OnOffStateValue);
        }
        else
        {
            GLHPWM_Set(PWM_CHANNEL_R, ((pow((float)(((float)s_u8LastR) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX))*s_u8Bn*s_u8OnOffStateValue);
        }
        
        if((s_u8LastG > 0) && (s_u8LastG <= SPECIAL_HANDLE_END))
        {
            GLHPWM_Set(PWM_CHANNEL_G, (pow((float)(((float)LOG_DIMMER_COLOR_MIN + ((float)s_u8LastG - (float)LOG_DIMMER_COLOR_MIN)*((float)SPECIAL_HANDLE_END - (float)LOG_DIMMER_COLOR_MIN) /\
                        (float)SPECIAL_HANDLE_END) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX)*s_u8Bn*s_u8OnOffStateValue);
        }
        else
        {
            GLHPWM_Set(PWM_CHANNEL_G, ((pow((float)(((float)s_u8LastG) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX))*s_u8Bn*s_u8OnOffStateValue);
        }
        
        if((s_u8LastB > 0) && (s_u8LastB <= SPECIAL_HANDLE_END))
        {
            GLHPWM_Set(PWM_CHANNEL_B, (pow((float)(((float)LOG_DIMMER_COLOR_MIN + ((float)s_u8LastB - (float)LOG_DIMMER_COLOR_MIN)*((float)SPECIAL_HANDLE_END - (float)LOG_DIMMER_COLOR_MIN) /\
                        (float)SPECIAL_HANDLE_END) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX)*s_u8Bn*s_u8OnOffStateValue);
        }  
        else
        {
            GLHPWM_Set(PWM_CHANNEL_B, ((pow((float)(((float)s_u8LastB) / GLH_COLOUR_MAX.0), GAMMA)*GLH_COLOUR_MAX))*s_u8Bn*s_u8OnOffStateValue); 
        }
    }
}

void GLHRGB_SetLogarithmicDimming(BOOL bIsSupportLogarithmicDimming)
{
    s_bIsLogarithmicDimming = bIsSupportLogarithmicDimming;
}